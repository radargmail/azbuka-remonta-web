server {
    listen [::]:80;
    listen 80;

    server_name app.azbuka-remonta.online;

    location ~ /.well-known/acme-challenge {
        allow all;
        root /var/www/certbot;
    }

    # redirect http to https www
    return 301 https://app.azbuka-remonta.online$request_uri;
}

server {
    listen [::]:443 ssl http2;
    listen 443 ssl http2;
    server_name app.azbuka-remonta.online;

    ssl_certificate /etc/letsencrypt/live/app.azbuka-remonta.online/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/app.azbuka-remonta.online/privkey.pem;
    include /etc/letsencrypt/options-ssl-nginx.conf;
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;

    index index.php index.html;

    root /var/www/public;

    access_log /etc/logs/nginx/nginx_access_backend.log;
    error_log /etc/logs/nginx/nginx_error_backend.log;

    client_max_body_size 300m;

    gzip on;
    gzip_comp_level 5;
    gzip_disable "msie6";
    gzip_types text/css application/json application/javascript;

    location ~* ^.+\.(jpg|jpeg|gif|png|ico|js|css)$ {
        expires 30s;
    }

    location / {
        try_files $uri $uri/ /index.php?_url=$uri&$args;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;

        fastcgi_pass backend:9000;

        fastcgi_index /index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_TRANSLATED $document_root$fastcgi_path_info;
        fastcgi_param PATH_INFO $fastcgi_path_info;
        fastcgi_param PHP_VALUE "upload_max_filesize=300M \n post_max_size=300M";
    }
}
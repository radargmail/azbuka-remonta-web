#!/bin/bash

echo "Hello from entrypoint"
echo "Checking vendor"

if [ ! -d vendor ]
then
    echo "There is no vendor directory"
    #composer install --prefer-dist --no-interaction
else
    echo "Vendor directory exists"
    composer run clear-cache
    #composer update --prefer-dist --no-interaction
fi

vendor/bin/phinx migrate -e default

/bin/echo -e "E3jZsgckdsjh\zY6hFkmk7" | passwd root && \

mkdir -p logs && touch logs/cron.log

export ARWEB_ENV
echo "env=$ARWEB_ENV"
echo "ARWEB_ENV=$ARWEB_ENV" >> mycron
cat /var/www/crontab >> mycron

crontab mycron && cron && rm mycron
echo "Started cron jobs"

chown -R www-data:www-data /var/www
echo "Changed owner project root dir"

service supervisor start
php-fpm7.4 -F
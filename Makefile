.PHONY: build prod-build test-start test-stop up prod-up start prod-start down prod-down stop prod-stop restart prod-restart logs prod-logs logs-web prod-logs-web ps psa info login migrate migration restart-supervisor
build:
	@docker-compose -f docker-compose.yml build $(c)
prod-build:
	@docker-compose -f docker-compose.prod.yml build $(c)
test-start:
	@docker-compose -f docker-compose-test-ci.yml up -d --build
test-stop:
	@docker-compose -f docker-compose-test-ci.yml down
up:
	@docker-compose -f docker-compose.yml up -d $(c)
prod-up:
	@docker-compose -f docker-compose.prod.yml up -d $(c)
start:
	@docker-compose -f docker-compose.yml start $(c)
prod-start:
	@docker-compose -f docker-compose.prod.yml start $(c)
down:
	@docker-compose -f docker-compose.yml down $(c)
prod-down:
	@docker-compose -f docker-compose.prod.yml down $(c)
stop:
	@docker-compose -f docker-compose.yml stop $(c)
prod-stop:
	@docker-compose -f docker-compose.prod.yml stop $(c)
restart:
	@docker-compose -f docker-compose.yml stop $(c)
	@docker-compose -f docker-compose.yml up -d $(c)
prod-restart:
	@docker-compose -f docker-compose.prod.yml stop $(c)
	@docker-compose -f docker-compose.prod.yml up -d $(c)
logs:
	@docker-compose -f docker-compose.yml logs --tail=100 -f $(c)
prod-logs:
	@docker-compose -f docker-compose.prod.yml logs --tail=100 -f $(c)
logs-web:
	@docker-compose -f docker-compose.yml logs --tail=100 -f arweb_backend
prod-logs-web:
	@docker-compose -f docker-compose.prod.yml logs --tail=100 -f arweb_backend
psa:
	@docker ps -a
ps:
	@docker ps
info:
	@echo "\033[32mChecking ARWeb Information\033[39m"
	@make testci-login cmd="php --version"
	@make testci-login cmd="php -r 'echo Phalcon\Version::get() . PHP_EOL;'"
	@make testci-login cmd="composer -V"
testci-login:
	@docker exec -it testci_backend /bin/bash -c "$(cmd)"
login:
	@docker exec -it arweb_backend /bin/bash -c "$(cmd)"
migrate:
	@docker exec -it arweb_backend ./vendor/bin/phinx migrate -e default
migration:
	@docker exec -it arweb_backend ./vendor/bin/phinx create $(name)
restart-supervisor:
	@docker exec -it arweb_backend supervisorctl restart all
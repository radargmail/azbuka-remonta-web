<?php

namespace App\Modules\Admin;

class Controller extends \App\Controller {

    protected $benchmark;

    public function initialize() {

        $this->tag::prependTitle('ARWEB© Admin | ');
        $this->addAssets();

    }

	public function addAssets(): void {

		$module = $this->dispatcher->getModuleName();
		$assetsModule = 'theme/' . $module . '/default';
		$controller = $this->router->getControllerName() ?: 'index';
		$action = $this->router->getActionName() ?: 'index';

		$currentPagePath = $module . '/' . $controller . '/' . $action;

		$cssCollections = $this->assets->createCollectionsByArray([
			'styles'        => [
				'path'  => 'cache/assets/' . $assetsModule,
				'filename' => 'sl',
				'items' => [
				],
			],
			'not_new_styles'     => [
				'path'  => 'cache/assets/' . $assetsModule,
				'items' => [
					'assets/' . $module . '/all_pages.css',
				]
			],
			'page_styles'        => [
				'target_filepath' => 'cache/assets/' . $currentPagePath . '.min.css',
				'items'           => [
					'assets/' . $currentPagePath . '.css',
					'assets/all_modules.css',
				],
			],
		], 'css');

		$jsCollections = $this->assets->createCollectionsByArray([
			'scripts'        => [
				'path'  => 'cache/assets/' . $assetsModule,
				'filename' => 'sh',
				'items' => [
				],
			],
			'page_scripts'        => [
				'target_filepath' => 'cache/assets/' . $currentPagePath . '.min.js',
				'items'           => [
					'assets/' . $currentPagePath . '.js',
					'assets/all_modules.js',
				],
			],
		], 'js');

		$collections = array_merge($cssCollections, $jsCollections);

		$this->assets->setCollections($collections);

	}

}
<?php

namespace App\Modules\User\Plugins;

use App\Injectable;
use App\Models\User;
use Phalcon\Events\Event;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Security\Random;

class LoginSecurity extends Injectable {

	const SESSION_NAME = 'arweb_user';
	public $uuid = false;

	public function beforeDispatch(Event $event, Dispatcher $dispatcher) {

		$this->uuid = (new Random())->uuid();

		$sessionUser = $this->getUserSession();

		if($sessionUser) {
			$this->di->set('user', $sessionUser[0]);
			$formatted_cost    = number_format($this->user->result_cost, 0, ' ', ' ');
			$result_cost_array = explode(' ', $formatted_cost);
			$pop               = array_pop($result_cost_array);
			array_push($result_cost_array, '<small class="text-muted font-weight-bold">' . $pop . '</small>');
			$this->user->formatted_cost = implode(' ', $result_cost_array);

			return true;
		}

		return false;
	}

	public function logout() {

		$this->session->remove(self::SESSION_NAME);

	}

	public function isLogin() {

		$sessionUser = $this->getUserSession();

		if($sessionUser) {

			if(isset($sessionUser['id'])) {

				$user = User::findFirst([
					'id = :id: AND status = 1',
					'bind' => [
						'id' => $sessionUser['id'],
					],
				]);

				if($user) {

					$this->di->set('user', $user);

					return true;

				}
			}
		}

		return false;
	}

	public function getUserSession() {

		if(!$this->session->has(self::SESSION_NAME)) {

			$this->uuid            = (new Random())->uuid();
			$identity              = new \stdClass();
			$identity->user        = new \stdClass();
			$identity->user->uuid  = $this->uuid;
			$identity->result_cost = 0;
			$this->session->set(self::SESSION_NAME, [
				$identity,
			]);

		}

		return $this->session->get(self::SESSION_NAME);
	}

}
<style>
    .list-inline-dotted .list-inline-item:not(:last-child):after {
        content: "⁞" !important;
        x-content: "⡇" !important;
        font-size: .7rem !important;
        opacity: .7 !important;
        margin-left: .5rem !important;
    }
    .list-inline-dotted .list-inline-item:not(:last-child) {
        margin-right: 0.4rem;
    }
    li.activated span {
        background: #625ABA 0% 0% no-repeat padding-box;
        width: 30px;
        height: 30px;
        color: white;
        border-radius: 50%;
    }
    .bottom-info {
        position: absolute;
        width: 100%;
        left: 0;
        border: 0;
        border-radius: 0 0 8px 8px;
        height: 50px;
        margin-top: -30px;
        padding-top: 17px;
        padding-left: 38px;
    }
    .badge-pill {
        padding: .4rem !important;
    }
    .badge-pill i {
        padding-top: 1px;
    }
    .ar li:not(.nav-item-open) .nav-link {
        color: #000;
    }
    .ar li.nav-item-open .nav-link {
        color: #625ABA;
    }
    .ar .nav-item-submenu {
        border: 1px solid #625ABA;
        border-radius: 8px;
    }
    .ar .nav-item:not(.nav-item-header):first-child {
        padding-top: 0;
    }
    .ar .nav-item-submenu:not(:first-child) {
        padding-top: 0px;
    }
    .ar .nav-item-submenu:not(:last-child) {
        padding-bottom: 0px;
    }
    .ar .nav-item:not(.nav-item-header):last-child {
        padding-bottom: 0px;
    }
    .ar .nav-item.bg-light {
        border-radius: 0 0 8px 8px;
    }
    .ar a.nav-link.text-black {
        border-bottom: 1px solid #e4e4e4;
    }
    .ar a.nav-link.active i {
        padding: 4px 5px;
        top: 4px;
        right: 18px;
        color: white;
        border-radius: 50%!important;
        background-color: #5c6bc0!important;
    }
    @media(min-width: 401px) {
	    .sidebar-panel {
            min-width: 147px;
        }
	    .service-panel {
            width:180px;
        }
    }
</style>


{% if action === 'roadmap' %}
	{% set display = 'd-none' %}
{% else %}
	{% set display = 'd-xl-none' %}
{% endif %}

<div class="sidebar-section sidebar-user my-1 {{ display }}">
	<div class="sidebar-section-body">
		<div class="media">
			<a href="#" class="mr-3">
				<img src="/assets/theme/user/default/favicon/favicon-32x32.png" class="rounded-circle" alt="">
			</a>

			<div class="media-body align-self-center">
				<div class="font-weight-semibold">AzbukaRemonta</div>
			</div>

			<div class="ml-3 align-self-center">
				<button type="button" class="btn btn-outline-light-100 text-black border-transparent btn-icon rounded-pill btn-sm sidebar-mobile-main-toggle d-xl-none">
					<i class="icon-cross2"></i>
				</button>
			</div>
		</div>
	</div>
</div>


<div class="sidebar-section" id="sidebar-ar">
	<div class="card border-0">
		<div class="">
			<div class="card-header d-sm-flex justify-content-sm-between align-items-sm-center pb-0">
				<h5 class="card-title">&nbsp;</h5>
				<div class="header-elements">
					<div class="list-icons">
						<a data-link="arweb-user" class="list-icons-item text-muted"><i class="far fa-user fa-2x"></i></a>
					</div>
				</div>
			</div>
			<div class="card-body">
				<h1 class="sidebar-resize-hide font-weight-black flex-1 mb-0">Мой ремонт</h1>
				<ul class="list-inline list-inline-dotted text-muted font-weight-normal font-size-sm">
					<li class="list-inline-item">{{ this.user.area }} м<sup>2</sup></li>
					<li class="list-inline-item">{{ this.user.rooms }}-к кв.</li>
					<li class="list-inline-item">{{ this.user.repair_type > 0 ? 'косметический' : 'капремонт' }}</li>
				</ul>
				<div class="d-flex">
					<div class="col-6 mr-0 pl-0 sidebar-panel">
						<a href="#">
							<div class="card text-white border-0" style="min-height: 117px;border-radius: 8px;box-shadow: 2px 2px 0px #00000014;background: #625ABA;">
								<div class="card-header text-right pt-0 pb-0">
									<div class="header-elements">
										<div class="list-icons">
											<i class="fa fa-long-arrow-alt-right" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="card-body pt-0 pb-0">
									<h3 style="font-size: calc(1.1rem + .1vw);" class="card-title pb-0 mb-0"><i class="fa fa-ruble-sign"></i> <strong>{{ this.user.formatted_cost }}</strong></h3>
									<p class="mb-3" style="white-space: nowrap;">предварительная<br>смета ремонта</p>
								</div>
							</div>
						</a>
					</div>
					<div class="col-6 mr-0 pr-0 sidebar-panel">
						<a href="#">
							<div class="card text-white border-0" style="min-height: 117px;border-radius: 8px;box-shadow: 2px 2px 0px #00000014;background: linear-gradient(104deg, #38C9AB, #218BD2);">
								<div class="card-header text-right pt-0 pb-0">
									<div class="header-elements">
										<div class="list-icons">
											<i class="fa fa-long-arrow-alt-right" aria-hidden="true"></i>
										</div>
									</div>
								</div>
								<div class="card-body pt-0 pb-0">
									<h3 style="font-size: calc(1.1rem + .1vw);" class="card-title pb-0 mb-0"><strong>0 </strong> <span class="font-weight-light">из</span> <strong> 25</strong></h3>
									<p class="mb-3" style="white-space: nowrap;">используемых<br>онлайн-сервисов</p>
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="card border-0 mb-0 pb-0">
		<div class="">
			<div class="card-body">
				<div class="media mb-3">
					<div class="media-body">
						<h2 class="mb-0 font-weight-bold">Дорожная карта</h2>
						<span class="text-muted font-size-base font-weight-normal">Индивидуальный план вашего ремонта.<br>Этапы раскрываются по клику:</span>
					</div>

					<div class="ml-3 align-top text-muted mt-1"><i class="fa fa-sliders-h fa-2x"></i></div>
				</div>
				<div class="">

					<ul class="nav nav-sidebar nav-sidebar-bordered nav-sidebar-icons-reverse ar" data-nav-type="accordion"> {#collapsible#}

						<li class="nav-item nav-item-submenu pb-0 mb-2 {% if controller == 'index' AND action != 'roadmap' %}nav-item-open{% endif %}">
							<a href="#" class="font-size-base font-weight-bold nav-link"><i class="text-muted font-size-base font-weight-light float-right">~30 дней</i>Подготовка</a>


							<ul class="nav nav-group-sub" style="display:  {% if controller == 'index' AND action != '' AND action != 'roadmap' %}block{% else %}none{% endif %};">
								<li class="nav-item">
									<a href="{{ url('index/plan') }}" class="nav-link font-weight-normal {% if controller == 'index' AND action == 'plan' %}active{% endif %} text-black">
										<i class="fa fa-long-arrow-alt-right"></i> Планировка</a>
								</li>
								<li class="nav-item">
									<a href="{{ url('index/design') }}" class="nav-link font-weight-normal {% if controller == 'index' AND action == 'design' %}active{% endif %} text-black">
										<i class="fa fa-long-arrow-alt-right"></i> Дизайн-проект
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ url('index/brigade') }}" class="nav-link font-weight-normal {% if controller == 'index' AND action == 'brigade' %}active{% endif %} text-black">
										<i class="fa fa-long-arrow-alt-right"></i> Выбор бригады
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ url('index/disassemble') }}" class="nav-link font-weight-normal {% if controller == 'index' AND action == 'disassemble' %}active{% endif %} text-black">
										<i class="fa fa-long-arrow-alt-right"></i> Демонтаж
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ url('index/garbage') }}" class="nav-link font-weight-normal {% if controller == 'index' AND action == 'garbage' %}active{% endif %} text-black">
										<i class="fa fa-long-arrow-alt-right"></i> Вывоз мусора
									</a>
								</li>
								<li class="nav-item">
									<a href="{{ url('index/redevelopment') }}" class="nav-link font-weight-normal {% if controller == 'index' AND action == 'redevelopment' %}active{% endif %} text-black">
										<i class="fa fa-long-arrow-alt-right"></i> Согласование планировки
									</a>
								</li>
								<li class="nav-item bg-light"><a href="{{ url('index/prepare') }}" class="nav-link">Что важно знать на этом этапе?</a></li>
							</ul>

						</li>

						<li class="nav-item nav-item-submenu pb-0 mb-2">
							<a href="#" class="font-size-base font-weight-bold nav-link"><i class="text-muted font-size-base font-weight-light float-right">~27 дней</i>Черновые работы</a>

							<ul class="nav nav-group-sub" style="display: none;">
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Межкомнатные перегородки</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Оштукатуривание стен</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Конструкции из гипсокартона</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Шпаклевка стен и потолков</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Стяжка пола</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Вентиляция и отопление</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Шумоизоляция</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Теплые полы</a></li>
								<li class="nav-item bg-light"><a data-link="arweb-user" class="nav-link">Что важно знать на этом этапе?</a></li>
							</ul>
						</li>

						<li class="nav-item nav-item-submenu pb-0 mb-2">
							<a href="#" class="font-size-base font-weight-bold nav-link"><i class="text-muted font-size-base font-weight-light float-right">~5 дней</i>Окна и двери</a>

							<ul class="nav nav-group-sub" style="display: none;">
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Замена окон</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Остекление балкона</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Подоконники</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Утепление лоджии</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Входная дверь</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Межкомнатные двери</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Раздвижные перегородки</a></li>
								<li class="nav-item bg-light"><a data-link="arweb-user" class="nav-link">Что важно знать на этом этапе?</a></li>
							</ul>
						</li>

						<li class="nav-item nav-item-submenu pb-0 mb-2">
							<a href="#" class="font-size-base font-weight-bold nav-link"><i class="text-muted font-size-base font-weight-light float-right">~6 дней</i>Сантехника</a>

							<ul class="nav nav-group-sub" style="display: none;">
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Выбор материалов</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Сборка коллектора</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Разводка труб</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Монтаж инсталляции</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Установка ванны</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Водонагреватель</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Аква-сторож</a></li>
								<li class="nav-item bg-light"><a data-link="arweb-user" class="nav-link">Что важно знать на этом этапе?</a></li>
							</ul>
						</li>

						<li class="nav-item nav-item-submenu pb-0 mb-2">
							<a href="#" class="font-size-base font-weight-bold nav-link"><i class="text-muted font-size-base font-weight-light float-right">~7 дней</i>Электрика</a>

							<ul class="nav nav-group-sub" style="display: none;">
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Выбор материалов</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Электротехнический план</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Сборка щитка</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Электропроводка</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Выключатели и розетки</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Электросчетчик</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Кондиционер</a></li>
								<li class="nav-item bg-light"><a data-link="arweb-user" class="nav-link">Что важно знать на этом этапе?</a></li>
							</ul>
						</li>

						<li class="nav-item nav-item-submenu pb-0 mb-2">
							<a href="#" class="font-size-base font-weight-bold nav-link"><i class="text-muted font-size-base font-weight-light float-right">~30 дней</i>Чистовые работы</a>

							<ul class="nav nav-group-sub" style="display: none;">
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Выбор материалов</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Окраска стен</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Поклейка обоев</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Укладка плитки</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Укладка пола</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Карнизы и плинтусы</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Натяжные потолки</a></li>
								<li class="nav-item bg-light"><a data-link="arweb-user" class="nav-link">Что важно знать на этом этапе?</a></li>
							</ul>
						</li>

						<li class="nav-item nav-item-submenu pb-0 mb-2">
							<a href="#" class="font-size-base font-weight-bold nav-link"><i class="text-muted font-size-base font-weight-light float-right">~7 дней</i>Мебель и техника</a>

							<ul class="nav nav-group-sub" style="display: none;">
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Кухонный гарнитур</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Диван</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Кровать и матрас</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Корпусная мебель</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Бытовая техника</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Умный дом</a></li>
								<li class="nav-item bg-light"><a data-link="arweb-user" class="nav-link">Что важно знать на этом этапе?</a></li>
							</ul>
						</li>

						<li class="nav-item nav-item-submenu pb-0 mb-2">
							<a href="#" class="font-size-base font-weight-bold nav-link"><i class="text-muted font-size-base font-weight-light float-right">~5 дней</i>Свет и декор</a>

							<ul class="nav nav-group-sub" style="display: none;">
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Выбор освещения</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Монтаж освещения</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Текстиль</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Растения</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Декор</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Прочее</a></li>
								<li class="nav-item bg-light"><a data-link="arweb-user" class="nav-link">Что важно знать на этом этапе?</a></li>
							</ul>
						</li>

						<li class="nav-item nav-item-submenu pb-0 mb-2">
							<a href="#" class="font-size-base font-weight-bold nav-link"><i class="text-muted font-size-base font-weight-light float-right">~7 дней</i>Переезд</a>

							<ul class="nav nav-group-sub" style="display: none;">
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Приемка работ</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Клининг</a></li>
								<li class="nav-item"><a data-link="arweb-user" class="nav-link font-weight-normal text-black"><i class="fa fa-long-arrow-alt-right"></i> Перевозка вещей</a></li>
								<li class="nav-item bg-light"><a data-link="arweb-user" class="nav-link">Что важно знать на этом этапе?</a></li>
							</ul>
						</li>
					</ul>

				</div>
			</div>
		</div>
	</div>
</div>
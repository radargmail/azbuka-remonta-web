<style>
    @media (min-width: 1200px) {
	    .sidebar {
		    min-width: 421px;
	    }
    }
    @media (min-width: 1400px) {
	    .sidebar {
		    min-width: 421px;
	    }
    }
    .sidebar-mobile-expanded {
	    width: initial !important;
    }
    .sidebar-content, .content-inner {
	    padding-right: 6px;
        overflow: hidden !important;
    }
    .sidebar-content:not(.ar-sidebar):hover, .content-inner:hover {
	    padding-right: 0px;
	    overflow-y: auto !important;
    }
</style>


{% if action === 'roadmap' %}
	{% set expanded = 'sidebar-mobile-expanded' %}
{% else %}
	{% set expanded = '' %}
{% endif %}

<div class="page-content col-12 ">

	<div class="sidebar bg-light sidebar-main sidebar-expand-xl {{ expanded }}">
		<div class="sidebar-content">
			{{ partial('layouts/parts/main_sidebar') }}
		</div>
	</div>

	<!-- Main content -->
	<div class="content-wrapper">

		<!-- Content area -->
		<div class="content pb-0 align-middle" style="display: contents;">

			<!-- Content area -->
			{{ flash.output() }}
			{{ content() }}
			<!-- /content area -->

		</div>

		<!-- Footer -->
		<div class="navbar d-none x-d-lg-flex w-100 navbar-light">
            <span class="navbar-text">
                <a href="/">AR</a>
            </span>

			<ul class="navbar-nav ml-lg-auto">
				<li class="nav-item"><a href="#" class="navbar-nav-link" target="_blank">{{ date('Y') }} <i class="fa fa-copyright ml-2 mr-2"></i> Все права защищены</a></li>
			</ul>
		</div>
		<!-- /footer -->

	</div>
	<!-- /main content -->

</div>
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="yandex-verification" content="" />
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keyword" content="">

    <link rel="apple-touch-icon" sizes="180x180" href="/assets/theme/user/default/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/theme/user/default/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/theme/user/default/favicon/favicon-16x16.png">
    <link rel="manifest" href="/assets/theme/user/default/favicon/site.webmanifest">
    <title>{{ getTitle() }}</title>

    {{ assets.outputCss('styles') }}
    {{ assets.outputCss() }}
    {{ assets.outputCss('page_styles') }}
    {{ assets.outputJs('head_scripts') }}

    <script>
        Url.set('{{ url() }}');
        Url.setStatic('{{ static_url() }}');
        Vue.options.delimiters = [ '<%', '%>' ];
        var offset = new Date().getTimezoneOffset();
        console.log(offset);
    </script>
</head>

<body class="custom-scrollbars">

{{ content() }}

{{ assets.outputJs('scripts') }}
{{ assets.outputJs() }}
{{ assets.outputJs('page_scripts') }}


{#{% if config.env === 'prod' %}#}
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        var ClientId = false;
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(71274733, 'init', {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true,
            trackHash:true,
            ecommerce:'dataLayer',
            userParams: { {% if this.user.user.id is defined %}UserID: "{{ this.user.user.id }}"{% endif %} }
        });

        ym(71274733, 'getClientID', function(clientID) {
            ClientId = clientID;
            if(!ClientId) {
                getClientId();
            }
        });

        function getClientId() {
            setTimeout(function () {
                ym(71274733, 'getClientID', function(clientID) {
                    ClientId = clientID;
                });
            }, 20)
        }
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/71274733" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
{#{% endif %}#}

</body>
</html>

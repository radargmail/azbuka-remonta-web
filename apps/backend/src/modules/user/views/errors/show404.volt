<div class="error-full_screen flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="clearfix">
                    <h1 class="float-left display-3 mr-4">404</h1>
                    <h4 class="pt-3">Упс! Вы потерялись.</h4>
                    <p class="text-muted">Страница, которую вы ищете, не найдена.</p>
                </div>
            </div>
        </div>
    </div>
</div>

<style>

    .error-full_screen {
        display: flex;
        flex-direction: column;
        min-height: 100vh;
    }

    .main .error-full_screen {
        min-height: 80vh;
    }
</style>
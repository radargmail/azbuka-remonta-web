<div class="content-inner">
	<div class="card bg-transparent border-0 shadow-none pl-1">
		<div class="d-flex d-xl-none">
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-transmission"></i>
			</button>
		</div>
		<div class="breadcrumb-line border-0 pt-3 font-size-lg font-weight-normal">
			<div class="breadcrumb breadcrumb-arrow">
				<a href="{{ url('index/roadmap') }}" class="breadcrumb-item">Дорожная карта</a>
				<span class="breadcrumb-item active">Подготовка</span>
			</div>
		</div>
	</div>

	<div class="timeline timeline-center pl-4 pr-4">
		<div class="timeline-container">

			<div class="timeline-date text-muted">
				<i class="icon-bookmark mr-2"></i> <span class="font-weight-semibold">Подготовка</span>, ~30 дней
			</div>

			<div class="timeline-row timeline-row-left">
				<div class="timeline-icon">
					<div class="bg-indigo text-white">
						<i class="icon-puzzle3"></i>
					</div>
				</div>

				<div class="timeline-time">
					Все что необходимо знать о <strong>планировке</strong>
					<div class="text-muted">8 уроков на 16 минут</div>
				</div>

				<div class="card border-left-3 border-left-info rounded-left-0">
					<div class="card-body">
						<div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
							<div class="col-9">
								<h6><a href="{{ url('index/plan') }}">Планировка</a></h6>
								<p class="mb-3">2D-конфигурация внутреннего пространства квартиры: габариты помещений, расположение в ней точек электрики, воды, света и габаритной мебели.</p>
							</div>

							<ul class="list list-unstyled text-right mb-0 mt-3 mt-sm-0 ml-auto">
								<li><span class="text-muted">0-800 руб/м<sup>2</sup></span></li>
								<li><span class="text-muted">4-10 дней</span></li>
								<li><span class="text-muted">5 сервисов</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="timeline-row timeline-row-right">
				<div class="timeline-icon">
					<div class="bg-indigo text-white">
						<i class="icon-design"></i>
					</div>
				</div>

				<div class="timeline-time">
					<strong>Дизайн-проект</strong> - что это и из чего состоит
					<div class="text-muted">8 уроков на 22 минуты</div>
				</div>

				<div class="card border-right-3 border-right-info rounded-right-0">
					<div class="card-body">
						<div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
							<div class="col-9">
								<h6><a href="{{ url('index/design') }}">Дизайн-проект</a></h6>
								<p class="mb-3">Чертежи, визуализации и подробные спецификации для детального понимания вида будущего пространства и продуманного расположения объектов.</p>
							</div>

							<ul class="list list-unstyled text-right mb-0 mt-3 mt-sm-0 ml-auto">
								<li><span class="text-muted">700-3000 руб/м<sup>2</sup></span></li>
								<li><span class="text-muted">25-30 дней</span></li>
								<li><span class="text-muted">5 сервисов</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="timeline-row timeline-row-left">
				<div class="timeline-icon">
					<div class="bg-indigo text-white">
						<i class="icon-hammer-wrench"></i>
					</div>
				</div>

				<div class="timeline-time">
					<strong>Выбор бригады</strong> - что нужно знать
					<div class="text-muted">8 уроков на 12 минут</div>
				</div>

				<div class="card border-left-3 border-left-info rounded-left-0">
					<div class="card-body">
						<div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
							<div class="col-9">
								<h6><a href="{{ url('index/brigade') }}">Выбор бригады</a></h6>
								<p class="mb-3">Есть два варианта работы со строителями: нанять одну бригаду на весь ремонт или подбирать мастеров на разные части работы, координируя их самостоятельно.</p>
							</div>

							<ul class="list list-unstyled text-right mb-0 mt-3 mt-sm-0 ml-auto">
								<li><span class="text-muted">3000-18000 руб/м<sup>2</sup></span></li>
								<li><span class="text-muted">10-18 дней</span></li>
								<li><span class="text-muted">5 сервисов</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="timeline-row timeline-row-right">
				<div class="timeline-icon">
					<div class="bg-indigo text-white">
						<i class="icon-construction"></i>
					</div>
				</div>

				<div class="timeline-time">
					<strong>Демонтаж</strong> - как это работает
					<div class="text-muted">6 уроков на 9 минут</div>
				</div>

				<div class="card border-right-3 border-right-info rounded-right-0">
					<div class="card-body">
						<div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
							<div class="col-9">
								<h6><a href="{{ url('index/disassemble') }}">Демонтаж</a></h6>
								<p class="mb-3">Подготовка квартиры с готовым ремонтом к новому ремонту. Перед началом следуюет договориться о вывозе мусора, предупредить соседей и оформить страховку.</p>
							</div>

							<ul class="list list-unstyled text-right mb-0 mt-3 mt-sm-0 ml-auto">
								<li><span class="text-muted">800-1100 руб/м<sup>2</sup></span></li>
								<li><span class="text-muted">3-8 дней</span></li>
								<li><span class="text-muted">5 сервисов</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="timeline-row timeline-row-left">
				<div class="timeline-icon">
					<div class="bg-indigo text-white">
						<i class="icon-trash"></i>
					</div>
				</div>

				<div class="timeline-time">
					<strong>Вывоз мусора</strong> - важный этап ремонта
					<div class="text-muted">5 уроков на 6 минут</div>
				</div>

				<div class="card border-left-3 border-left-info rounded-left-0">
					<div class="card-body">
						<div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
							<div class="col-9">
								<h6><a href="{{ url('index/garbage') }}">Вывоз мусора</a></h6>
								<p class="mb-3">Утилизация строительного мусора потребуется на протяжении всего ремонта. Стоит договориться с надежным подрядчиком до его начала.</p>
							</div>

							<ul class="list list-unstyled text-right mb-0 mt-3 mt-sm-0 ml-auto">
								<li><span class="text-muted">500-1500 руб/м<sup>2</sup></span></li>
								<li><span class="text-muted">во время ремонта</span></li>
								<li><span class="text-muted">3 сервиса</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="timeline-row timeline-row-right">
				<div class="timeline-icon">
					<div class="bg-indigo text-white">
						<i class="icon-task"></i>
					</div>
				</div>

				<div class="timeline-time">
					Детали, тонкости и риски при <strong>перепланировке и согласовании</strong>
					<div class="text-muted">8 уроков на 16 минут</div>
				</div>

				<div class="card border-right-3 border-right-info rounded-right-0">
					<div class="card-body">
						<div class="d-sm-flex align-item-sm-center flex-sm-nowrap">
							<div class="col-9">
								<h6><a href="{{ url('index/кувумудщзьуте') }}">Согласование планировки</a></h6>
								<p class="mb-3">Правильная перепланировка поможет превратить квартиру в серийном доме в пространство, удобное именно для вас, и в то же время учесть жилищные нормативы.</p>
							</div>

							<ul class="list list-unstyled text-right mb-0 mt-3 mt-sm-0 ml-auto">
								<li><span class="text-muted">400-1200 руб/м<sup>2</sup></span></li>
								<li><span class="text-muted">10-40 дней</span></li>
								<li><span class="text-muted">4 сервиса</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<div class="col-lg-12 col-xl-8 offset-xl-2 app-cont">
	<div id="construct" class="row">
		<div class="col-lg-5">
			<div class="card bg-transparent">
				<div class="card-header">
					Настраиваем вашего помощника
					<h4 class="font-weight-bold">Уточним параметры ремонта:</h4>
				</div>
				<div class="card-body">
					<form method="post" enctype="multipart/form-data">
						<div class="switch">
							<input type="radio" class="switch-input" id="novostroika" name="estate_type" value="0" v-model="estate_type" />
							<label for="novostroika" class="switch-label switch-label-off">Новостройка</label>
							<input type="radio" class="switch-input" id="vtorichka" name="estate_type" value="1" v-model="estate_type" />
							<label for="vtorichka" class="switch-label switch-label-on">Вторичка</label>
							<span class="switch-selection"></span>
						</div>
						<div class="switch">
							<input type="radio" class="switch-input" id="kapital" name="repair_type" value="0" v-model="repair_type" />
							<label for="kapital" class="switch-label switch-label-off">Капитальный</label>
							<input type="radio" class="switch-input" id="cosmo" name="repair_type" value="1" v-model="repair_type" />
							<label for="cosmo" class="switch-label switch-label-on">Косметический</label>
							<span class="switch-selection"></span>
						</div>
						<div class="switch">
							<input type="radio" class="switch-input" id="standart" name="design_type" value="0" v-model="design_type" />
							<label for="standart" class="switch-label switch-label-off">Стандартный</label>
							<input type="radio" class="switch-input" id="designed" name="design_type" value="1" v-model="design_type" />
							<label for="designed" class="switch-label switch-label-on">Дизайнерский</label>
							<span class="switch-selection"></span>
						</div>
						<div class="switch">
							<input type="radio" class="switch-input" id="soon" name="start_type" value="0" v-model="start_type" />
							<label for="soon" class="switch-label switch-label-off">Скоро начнем</label>
							<input type="radio" class="switch-input" id="timeago" name="start_type" value="1" v-model="start_type" />
							<label for="timeago" class="switch-label switch-label-on">Через 3-6 мес.</label>
							<span class="switch-selection"></span>
						</div>
						<div class="switch">
							<input type="radio" class="switch-input" id="handsome" name="worker_type" value="0" v-model="worker_type" />
							<label for="handsome" class="switch-label switch-label-off">Самостоятельно</label>
							<input type="radio" class="switch-input" id="workers" name="worker_type" value="1" v-model="worker_type" />
							<label for="workers" class="switch-label switch-label-on">С рабочими</label>
							<span class="switch-selection"></span>
						</div>
						<div class="form-group form-group-feedback form-group-feedback-left" style="margin:8px auto;">
							<input type="number" step="any" placeholder="33,0" id="area" v-model="area" name="area" required class="form-control form-control-lg text-right border-0" style="color:#5118df;border-radius:7px;padding-left:7rem;height: 50px;">
							<div class="form-control-feedback form-control-feedback-lg" style="top: 4px; color: #7a7a7a;">
								Площадь, м<sup>2</sup>
							</div>
						</div>
						<div class="form-group form-group-feedback form-group-feedback-left" style="margin:8px auto;">
							<div class="switch-field mini form-control form-control-lg text-right d-block border-0" style="border-radius:7px;x-padding-left:7rem;height: 50px;">
								<input type="radio" id="one-k" name="rooms" value="1" v-model="rooms" />
								<label for="one-k">1</label>
								<input type="radio" id="two-k" name="rooms" value="2" v-model="rooms" />
								<label for="two-k">2</label>
								<input type="radio" id="three-k" name="rooms" value="3" v-model="rooms" />
								<label for="three-k">3</label>
								<input type="radio" id="four-k" name="rooms" value="4" v-model="rooms" />
								<label for="four-k">4</label>
							</div>
							<div class="form-control-feedback form-control-feedback-lg" style="top: 4px; color: #7a7a7a;">
								Комнат:
							</div>
						</div>
						<div class="form-control bg-transparent border-0" style="margin:20px auto;padding-right:0">
							<input type="hidden" id="uuid" name="uuid" v-model="uuid" :value="uuid" />
							<input type="hidden" id="ym_id" name="ym_id" v-model="ym_id" :value="ym_id" />
							<button type="submit" class="btn btn-success float-right">Далее</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-lg-7 d-none d-lg-block">
			<div class="card-header border-0">
				<h4 class="ml-3 pt-3 font-weight-semibold">Через 15 секунд Вы получите:</h4>
			</div>
			<div class="card-body">
				<ul class="list list-unstyled ml-3">
					<li><i class="far fa-check-circle text-success mr-2"></i> Индивидуальную дорожную карту вашего ремонта</li>
					<li><i class="far fa-check-circle text-success mr-2"></i> Предварительный расчет его стоимости</li>
					<li><i class="far fa-check-circle text-success mr-2"></i> Ёмкий инфо-гид по всем этапам работ</li>
					<li><i class="far fa-check-circle text-success mr-2"></i> Доступ к удобным онлайн-сервисам
						<ul class="list list-unstyled ml-4">
							<li><i class="fa fa-minus text-success mr-2"></i> составление полной сметы</li>
							<li><i class="fa fa-minus text-success mr-2"></i> расчет черновых материалов</li>
							<li><i class="fa fa-minus text-success mr-2"></i> консультации по планировке</li>
							<li><i class="fa fa-minus text-success mr-2"></i> поиск профильных специалистов</li>
							<li><i class="fa fa-minus text-success mr-2"></i> и еще 20 бесплатных сервисов</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
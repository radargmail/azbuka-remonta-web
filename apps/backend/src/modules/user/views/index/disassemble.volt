<style>
    .page-title {
        padding-top: 0;
        padding-bottom: 0;
    }

    .card.ar-card #collapseExample.collapse:not(.show) {
        display: block;
        height: 1.7rem;
        overflow: hidden;
    }

    .card.ar-card #collapseExample.collapsing {
        height: 1.7rem;
    }

    .card.ar-card a.collapsed::after {
        content: 'Подробнее...';
    }

    .card.ar-card a[data-toggle="collapse"]:not(.collapsed)::after {
        content: 'Скрыть';
    }

    .ar-butts {
        color: white;
        background: linear-gradient(104deg, #38C9AB, #218BD2);
        border: 0;
        border-radius: 8px !important;
    }

    .card-group {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-flow: row wrap;
        flex-flow: row wrap;
    }

    .content-inner .card {
        border-radius: 8px;
    }

    .card .card-body .tab-pane:not(.show) {

    }

    .ar-tabs .nav-item {
        border-top: 1px solid #e4e4e4;
    }

    .ar-tabs a.nav-link.active i {
        padding: 4px 5px;
        top: 8px;
        right: 18px;
        color: white;
        border-radius: 50% !important;
        background-color: #5c6bc0 !important;
    }

    .ar-tabs a.nav-link.active[href="#soon"] i {
        padding: 4px 5px;
        top: 8px;
        right: 18px;
        color: #000;
        border-radius: 50% !important;
        background-color: #e7e7e7 !important;
    }

    .ar-tab-card-body.card-body {
        font-weight: inherit !important;
        font-size: inherit !important;
    }

    @media (max-width: 992px) {
        .ar-tab-card-body {
            height: 89vh;
            overflow: auto;
        }
    }

    @media (min-width: 992px) {
        .ar-r-sidebar {
            width: inherit !important;
        }
    }
</style>

<div class="content-inner">
	<div class="card bg-transparent border-0 shadow-none pl-1">
		<div class="d-flex d-xl-none">
			<button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
				<i class="icon-transmission"></i>
			</button>
		</div>
		<div class="breadcrumb-line border-0 pt-3 font-size-lg font-weight-normal">
			<div class="breadcrumb breadcrumb-arrow">
				<a href="{{ url('index/roadmap') }}" class="breadcrumb-item">Дорожная карта</a>
				<a href="{{ url('index/prepare') }}" class="breadcrumb-item">Подготовка</a>
				<span class="breadcrumb-item active">Демонтаж</span>
			</div>
		</div>

		<div class="page-header-content">
			<div class="page-title">
				<h1 class="font-weight-black">Демонтаж</h1>
			</div>
		</div>

		<div class="card ar-card bg-transparent border-0 shadow-none">

			<div class="card-body">
				<span class="badge badge-flat badge-pill border-0 bg-white text-muted">
					<span class="bg-indigo rounded-circle text-white" style="padding: 2px 4px;"><i
								class="fa fa-ruble-sign" style="font-size: xx-small;"></i></span>
					<span class="mr-2 ml-2">800-1100 руб./м<sup>2</sup></span>
				</span>
				<span class="badge badge-flat badge-pill border-0 bg-white text-muted">
					<span class="bg-indigo rounded-circle text-white" style="padding: 2px 4px;"><i
								class="fa fa-hourglass" style="font-size: xx-small;"></i></span>
					<span class="mr-2 ml-2">3-8 дней</span>
				</span>

				<div class="row p-3">
					<p class="collapse font-weight-semibold" id="collapseExample" aria-expanded="false"
					   style="font-size: initial;">
						Подготовка квартиры с готовым ремонтом к новому ремонту. Перед началом следуюет договориться о
						вывозе мусора, предупредить соседей и оформить страховку.<br>
						Частичный демонтаж включает удаление чистовых покрытий (обоев, плитки, шпаклевки).
						Полный затрагивает стеновые, потолочные и половые конструкции, а также сантехнические,
						отопительные элементы, оконные блоки и двери.
					</p>
					<a role="button" class="collapsed" data-toggle="collapse" href="#collapseExample"
					   aria-expanded="false" aria-controls="collapseExample"></a>
				</div>

				<div class="row p-3">
					<h3 class="font-weight-bold">Онлайн-сервисы для этого этапа</h3>
				</div>

				<div class="row p-3">
					<div class="card-group mb-sm-3">
						<div class="card mr-2 bg-transparent shadow-none border-0 service-panel" style="height: 113px;">
							<div class="card-body p-0 m-0">
								<div class="card ar-butts text-right" style="margin-bottom: 13px">
									<a data-link="arweb-user">
										<div class="card-body pt-1 pb-0">
											<p class="float-left text-left mb-1">Чек-лист<br>этапа</p>
											<i class="fa fa-tasks fa-2x pt-1 float-right"></i>
										</div>
									</a>
								</div>
								<div class="card mb-0 ar-butts text-right">
									<a data-link="arweb-user">
										<div class="card-body pt-1 pb-0">
											<p class="float-left text-left mb-1">Вопрос<br>эксперту</p>
											<i class="icon-comment-discussion icon-2x pt-1 float-right"></i>
										</div>
									</a>
								</div>
							</div>
						</div>
						<div class="card mr-2 ar-butts text-right service-panel" style="height: 113px;">
							<a data-link="arweb-user">
								<div class="card-body pb-0">
									<i class="fa fa-ruler-combined fa-2x mb-2"></i>
									<p class="mb-0 pb-1 text-left">Подбор бригады<br>для демонтажа</p>
								</div>
							</a>
						</div>
						<div class="card mr-2 ar-butts text-right service-panel" style="height: 113px;">
							<a data-link="arweb-user">
								<div class="card-body pb-0">
									<i class="fa fa-ruler-combined fa-2x mb-2"></i>
									<p class="mb-0 pb-1 text-left">Согласование<br>плана демонтажа</p>
								</div>
							</a>
						</div>
						<div class="card ar-butts text-right service-panel" style="height: 113px;">
							<a data-link="arweb-user">
								<div class="card-body pb-0">
									<i class="fa fa-ruler-combined fa-2x mb-2"></i>
									<p class="mb-0 pb-1 text-left">Страхование на<br>время демонтажа</p>
								</div>
							</a>
						</div>
					</div>
				</div>

			</div>
		</div>

		<div class="row pl-4 pr-4">
			<div class="col-lg-4">
				<div class="card">
					<div class="card-body">
						<div class="media mb-3">
							<div class="media-body">
								<h2 class="mb-0 font-weight-bold">Мини-учебник</h2>
								<span class="text-muted font-size-base font-weight-normal">6 уроков на 9 минут</span>
							</div>
						</div>
						<ul class="nav d-lg-none nav-sidebar ar-tabs nav-sidebar-icons-reverse">
							<li class="nav-item"><a data-link="arweb-user" class="nav-link text-black pl-0"
							                        data-toggle="tab"><i class="fa fa-long-arrow-alt-right"></i>
									Составление плана демонтажа</a></li>
							<li class="nav-item"><a data-link="arweb-user"
							                        class="nav-link text-black pl-0 navbar-toggler sidebar-mobile-right-toggle"
							                        data-toggle="tab"><i class="fa fa-long-arrow-alt-right"></i> Выбор
									бригады для проведения работ</a></li>
							<li class="nav-item"><a data-link="arweb-user" class="nav-link text-black pl-0"
							                        data-toggle="tab"><i class="fa fa-long-arrow-alt-right"></i>
									Организация вывоза мусора</a></li>
							<li class="nav-item"><a data-link="arweb-user" class="nav-link text-black pl-0"
							                        data-toggle="tab"><i class="fa fa-long-arrow-alt-right"></i> Как
									составить договор на демонтаж</a></li>
							<li class="nav-item"><a data-link="arweb-user" class="nav-link text-black pl-0"
							                        data-toggle="tab"><i class="fa fa-long-arrow-alt-right"></i> Правила
									и ограничения демонтажа</a></li>
							<li class="nav-item"><a data-link="arweb-user" class="nav-link text-black pl-0"
							                        data-toggle="tab"><i class="fa fa-long-arrow-alt-right"></i> Почему
									стоит познакомиться с дворником</a></li>
						</ul>
						<ul class="nav d-lg-block d-none nav-sidebar ar-tabs nav-sidebar-icons-reverse">
							<li class="nav-item"><a data-link="arweb-user" class="nav-link text-black pl-0"
							                        data-toggle="tab"><i class="fa fa-long-arrow-alt-right"></i>
									Составление плана демонтажа</a></li>
							<li class="nav-item"><a data-link="arweb-user" class="nav-link text-black pl-0"
							                        data-toggle="tab"><i class="fa fa-long-arrow-alt-right"></i> Выбор
									бригады для проведения работ</a></li>
							<li class="nav-item"><a data-link="arweb-user" class="nav-link text-black pl-0"
							                        data-toggle="tab"><i class="fa fa-long-arrow-alt-right"></i>
									Организация вывоза мусора</a></li>
							<li class="nav-item"><a data-link="arweb-user" class="nav-link text-black pl-0"
							                        data-toggle="tab"><i class="fa fa-long-arrow-alt-right"></i> Как
									составить договор на демонтаж</a></li>
							<li class="nav-item"><a data-link="arweb-user" class="nav-link text-black pl-0"
							                        data-toggle="tab"><i class="fa fa-long-arrow-alt-right"></i> Правила
									и ограничения демонтажа</a></li>
							<li class="nav-item"><a data-link="arweb-user" class="nav-link text-black pl-0"
							                        data-toggle="tab"><i class="fa fa-long-arrow-alt-right"></i> Почему
									стоит познакомиться с дворником</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-7">
				<div class="sidebar ar-r-sidebar sidebar-right sidebar-expand-lg{# sidebar-mobile-expanded #}">

					<!-- Expand button -->
					<button type="button" class="d-lg-none navbar-toggler sidebar-mobile-right-toggle bg-light">
						<i class="icon-arrow-right8"></i>
					</button>
					<!-- /expand button -->

					<!-- Sidebar content -->
					<div class="sidebar-content ar-sidebar">

						<div class="tab-content flex-column flex-xl-fill font-weight-normal overflow-auto">
							<div class="tab-pane fade" id="soon">
								<div class="card mb-0">
									<div class="card-body ar-tab-card-body d-flex">
										<div class="m-auto text-muted">
											Раздел находится в стадии наполнения!
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
					<!-- /sidebar content -->

				</div>
			</div>
		</div>
	</div>
</div>
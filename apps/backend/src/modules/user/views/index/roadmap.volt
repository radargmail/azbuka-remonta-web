
<style>

    @media (min-width: 1200px) {
        .sidebar-mobile-expanded {
            width: 18.75rem !important;
        }
    }
    @media (max-width: 1199.98px) {
        .sidebar-mobile-expanded {
            width: initial !important;
        }
    }
</style>

<div class="content-inner">
	<div class="col-lg-12 d-none d-lg-flex text-center align-self-center text-muted">
		<div class="m-auto d-flex" style="width: 300px">
			<i class="icon icon-touch icon-2x m-auto"></i>
			<span class="text-left ml-2">Выберите интересующий этап в дорожной карте слева</span>
		</div>
	</div>
</div>
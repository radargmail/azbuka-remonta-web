<?php

namespace App\Modules\User\Controllers;

use App\Models\EstimateTemp;
use App\Models\User;
use App\Models\UserYm;
use App\Modules\User\Controller;
use Phalcon\Mvc\Model\Transaction\Manager as TransactionManager;
use Phalcon\Security\Random;

class IndexController extends Controller {

	const SESSION_NAME = 'arweb_user';

	public function initialize() {
		parent::initialize();
	}

	public function mainAction() {

		$this->view->setTemplateBefore('index');

		if($this->request->isPost()) {

			if($this->session->get(self::SESSION_NAME)) {

				$this->response->redirect('index/roadmap');

			} else {
				$this->flash->error('Ошибка');
			}

		}

	}

	public function roadmapAction() {
		$this->view->setTemplateBefore('main');
	}

	public function prepareAction() {
		$this->view->setTemplateBefore('main');
	}

	public function planAction() {
		$this->view->setTemplateBefore('main');
	}

	public function designAction() {
		$this->view->setTemplateBefore('main');
	}

	public function redevelopmentAction() {
		$this->view->setTemplateBefore('main');
	}

	public function brigadeAction() {
		$this->view->setTemplateBefore('main');
	}

	public function disassembleAction() {
		$this->view->setTemplateBefore('main');
	}

	public function garbageAction() {
		$this->view->setTemplateBefore('main');
	}

	public function testResultCostAction() {
		$this->view->disable();

		$identity = $this->session->get(self::SESSION_NAME)[0];
		dump($identity);

		($identity->repair_type > 0) ? $start_cost = 14900 : $start_cost = 34900;
		($identity->area > 0) ? $area_cost = ($start_cost * $identity->area) : $area_cost = 0;
		($identity->estate_type > 0) ? $estate_cost = 1 : $estate_cost = 1.1;
		($identity->worker_type > 0) ? $worker_cost = 1.3 : $worker_cost = 1;
		($identity->design_type > 0) ? $design_cost = 1.3 : $design_cost = 1;

		dump($identity->user->uuid, $start_cost, $area_cost, $estate_cost, $worker_cost, $design_cost, $this->resultCost($identity));
		die;
	}

	public function resultCostAction() {

		$this->view->disable();

		if($this->request->isPost()) {

			$identity              = new \stdClass();
			$identity->user        = new \stdClass();
			$identity->user->uuid  = $this->request->getPost('user')['uuid'] ?: $this->login_security->uuid;
			$identity->estate_type = $this->request->getPost('estate_type');
			$identity->repair_type = $this->request->getPost('repair_type');
			$identity->start_type  = $this->request->getPost('start_type');
			$identity->worker_type = $this->request->getPost('worker_type');
			$identity->design_type = $this->request->getPost('design_type');
			$identity->area        = $this->request->getPost('area');
			$identity->rooms       = $this->request->getPost('rooms');
			$identity->user->ym_id = $this->request->getPost('user')['ym_id'];

			// ceil() - к большему целому значению
			// floor() - к меньшему целому значению
			// round() - математически к ближайшему
			$identity->result_cost = round($this->resultCost($identity), 0);

			if($identity->result_cost > 0) {

				$this->session->set(self::SESSION_NAME, [
					$identity,
				]);

				$user_ym = UserYm::findFirstByYmClientId($identity->user->ym_id);

				$manager     = new TransactionManager();
				$transaction = $manager->get();

				if(!$user_ym) {

					/*$user = new User();
					$user->setTransaction($transaction);
					$user->ym_client_id = $identity->user->ym_id;
					$user->login        = $identity->user->ym_id;
					$user->password     = $user->generateSymbolsString(8, User::GEN_PASS, true);
					$user->last_name    = $identity->user->ym_id;
					$user->first_name   = $identity->user->ym_id;
					$user->email        = $identity->user->ym_id;
					$user->phone        = $identity->user->ym_id;

					if(!$user->save()) {
						$transaction->rollback('Не удалось сохранить Пользователя');
					}*/

					$user_ym = new UserYm();
					$user_ym->setTransaction($transaction);
					$user_ym->ym_client_id = $identity->user->ym_id;

					if(!$user_ym->save()) {
						$transaction->rollback('Не удалось сохранить Яндекс Клиента');
					}
				}

				$estimate = new EstimateTemp();
				$estimate->setTransaction($transaction);
				$estimate->estate_type = $identity->estate_type;
				$estimate->repair_type = $identity->repair_type;
				$estimate->start_type  = $identity->start_type;
				$estimate->worker_type = $identity->worker_type;
				$estimate->design_type = $identity->design_type;
				$estimate->area        = $identity->area;
				$estimate->rooms       = $identity->rooms;
				$estimate->user_ym_id  = $user_ym->id;
				if(!$estimate->save()) {
					$transaction->rollback('Не удалось сохранить Отчет');
				}

				$transaction->commit();
				return $this->sendJson($identity->result_cost);

			}

			return $this->errorJson('Ошибка!');

		}

		return $this->errorJson('Wrong request!');

	}

	private function resultCost($identity = false) {
		$identity ?: $this->session->get(self::SESSION_NAME)[0];
		($identity->repair_type > 0) ? $start_cost = 14900 : $start_cost = 34900;
		($identity->area > 0) ? $area_cost = ($start_cost * $identity->area) : $area_cost = 0;
		($identity->estate_type > 0) ? $estate_cost = 1 : $estate_cost = 1.1;
		($identity->worker_type > 0) ? $worker_cost = 1.3 : $worker_cost = 1;
		($identity->design_type > 0) ? $design_cost = 1.3 : $design_cost = 1;
		return $area_cost * $estate_cost * $worker_cost * $design_cost;
	}

	public function genUuidAction(): string {
		$this->view->disable();
		return $this->login_security->uuid ?: (new Random())->uuid();
	}

	public function addPhoneAction() {

		$this->view->disable();

		if($this->request->isPost()) {

			$search = UserYm::findFirstByYmClientId($this->user->user->ym_id);

			if($search) {
				if(isset($search->user_id)) {
					$search->User->phone = $this->request->getPost('phone');
					if(!$search->User->save()) {
						throw new \Exception($search->User->getMessages()[0]->getMessage());
					}

				} else {

					$user               = new User();
					$user->ym_client_id = $this->user->user->ym_id;
					$user->login        = $this->user->user->ym_id;
					$user->password     = $user->generateSymbolsString(8, User::GEN_PASS, true);
					$user->last_name    = $this->user->user->ym_id;
					$user->first_name   = $this->user->user->ym_id;
					$user->email        = $this->user->user->ym_id;
					$user->phone        = $this->request->getPost('phone');

					if(!$user->save()) {
						throw new \Exception($user->getMessages()[0]->getMessage());
					}

					$search->user_id      = $user->id;

					if(!$search->save()) {
						throw new \Exception($search->getMessages()[0]->getMessage());
					}

				}

			} else {

				$user               = new User();
				$user->ym_client_id = $this->user->user->ym_id;
				$user->login        = $this->user->user->ym_id;
				$user->password     = $user->generateSymbolsString(8, User::GEN_PASS, true);
				$user->last_name    = $this->user->user->ym_id;
				$user->first_name   = $this->user->user->ym_id;
				$user->email        = $this->user->user->ym_id;
				$user->phone        = $this->request->getPost('phone');

				if(!$user->save()) {
					throw new \Exception($user->getMessages()[0]->getMessage());
				}

				$user_ym               = new UserYm();
				$user_ym->ym_client_id = $this->user->user->ym_id;
				$user_ym->user_id      = $user->id;

				if(!$user_ym->save()) {
					throw new \Exception($user_ym->getMessages()[0]->getMessage());
				}

			}

			$identity = $this->user;
			$identity->user->phone = $this->request->getPost('phone');
			$this->session->remove(self::SESSION_NAME);
			$this->session->set(self::SESSION_NAME, [
				$identity,
			]);

			return $this->successJson('ok');

		}

		return $this->errorJson('Wrong request!');

	}

}
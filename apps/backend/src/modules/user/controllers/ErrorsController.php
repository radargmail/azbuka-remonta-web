<?php

namespace App\Modules\User\Controllers;


use App\Modules\User\Controller;

class ErrorsController extends Controller {

	public function show404Action(): void {
		$this->response->setStatusCode(404);
	}

}
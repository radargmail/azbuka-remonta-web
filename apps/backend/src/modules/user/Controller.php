<?php

namespace App\Modules\User;

use Carbon\Carbon;
use Jenssegers\Agent\Agent;

class Controller extends \App\Controller {

	protected $agent;
	protected $module;
	protected $controller;
	protected $action;

    public function initialize() {

	    $this->agent = new Agent();
        $this->tag::prependTitle('ARWEB© | ');
	    $this->module = $this->dispatcher->getModuleName() ?: 'user';
	    $this->controller = $this->router->getControllerName() ?: 'index';
	    $this->action = $this->router->getActionName() ?: 'index';
        $this->addAssets();

		$this->view->module = $this->module;
		$this->view->controller = $this->controller;
		$this->view->action = $this->action;

    }

	public function indexAction() {

		$this->view->disable();

		return $this->sendJson(
			[
				'app' => 'Hello World',
				'time' => Carbon::now('Europe/Moscow'),
				'languages' => $this->agent->languages(),
				'device' => $this->agent->device(),
				'platform' => $this->agent->platform(),
				'platform_version' => $this->agent->version($this->agent->platform()),
				'browser' => $this->agent->browser(),
				'browser_version' => $this->agent->version($this->agent->browser()),
				'isMobile' => $this->agent->isMobile(),
				'isTablet' => $this->agent->isTablet(),
				'isDesktop' => $this->agent->isDesktop(),
				'isPhone' => $this->agent->isPhone(),
				'isRobot' => $this->agent->isRobot(),
			]
		);

	}

	public function addAssets(): void {

		$assetsModule = 'theme/' . $this->module . '/default';

		$currentPagePath = $this->module . '/' . $this->controller . '/' . $this->action;

		$cssCollections = $this->assets->createCollectionsByArray([
			'styles'        => [
				'path'  => 'cache/assets/' . $assetsModule,
				'filename' => 'sl',
				'items' => [
					'https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900',
					'assets/css/icons/icomoon/styles.min.css',
					'assets/css/icons/fontawesome/styles.min.css',
					'assets/theme/user/default/css/bootstrap.min.css',
					'assets/theme/user/default/css/bootstrap_limitless.min.css',
					//'assets/theme/user/default/css/all.min.css',
					'assets/theme/user/default/css/layout.css',
					//'assets/theme/user/default/css/layout.min.css',
					'assets/theme/user/default/css/components.min.css',
				],
			],
			'page_styles'        => [
				'target_filepath' => 'cache/assets/' . $currentPagePath . '.min.css',
				'items'           => [
					'assets/css/main.css',
					'assets/' . $currentPagePath . '.css',
				],
			],
		], 'css');

		$jsCollections = $this->assets->createCollectionsByArray([
			'head_scripts'        => [
				'path'  => 'cache/assets/' . $assetsModule,
				'filename' => 'shp',
				'items' => [
					'assets/js/main/jquery.min.js',
					'assets/js/main/bootstrap.bundle.min.js',
					'assets/js/main/pace.min.js',
					'assets/js/ejs/v2.6.2/ejs.min.js',
					'assets/js/vue/2.6.10/vue.min.js',
					'assets/js/Url.js',
					'assets/js/BootstrapModal.js',
					'assets/components/vue/BootstrapModal.js',
					'assets/js/plugins/inputmask.js',
					'assets/js/Phone.js',
					'assets/theme/user/default/js/app.js',
				],
			],
			'scripts'        => [
				'path'  => 'cache/assets/' . $assetsModule,
				'filename' => 'sh',
				'items' => [
				],
			],
			'page_scripts'        => [
				'target_filepath' => 'cache/assets/' . $currentPagePath . '.min.js',
				'items'           => [
					'assets/' . $currentPagePath . '.js',
				],
			],
		], 'js');

		$collections = array_merge($cssCollections, $jsCollections);

		$this->assets->setCollections($collections);

	}

}
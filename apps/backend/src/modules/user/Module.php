<?php

namespace App\Modules\User;

//use App\Modules\Admin\Plugins\Elements;
use App\Modules\User\Plugins\LoginSecurity;
use Carbon\Carbon;
use Phalcon\Di\DiInterface;
use Phalcon\Loader;
use Phalcon\Mvc\ModuleDefinitionInterface;
use Phalcon\Url;

class Module implements ModuleDefinitionInterface {

    public function registerAutoloaders(DiInterface $di = null) {

        $loader = new Loader();

        $loader->registerNamespaces([
            __NAMESPACE__ . '\Controllers'         => __DIR__ . '/controllers/',
            __NAMESPACE__ . '\Plugins'             => __DIR__ . '/plugins/',
            __NAMESPACE__ . '\Forms'               => __DIR__ . '/forms/',
            __NAMESPACE__ . '\Structures'          => __DIR__ . '/structures/',
            __NAMESPACE__ . '\Structures\Request'  => __DIR__ . '/structures/request/',
            __NAMESPACE__ . '\Structures\Response' => __DIR__ . '/structures/response/',
        ], true);

        $loader->register();

    }

    public function registerServices(DiInterface $di = null) {

        if($di){

            $view = $di->get('view');
            $view->setViewsDir(__DIR__ . '/views');
            $di->set('view', $view);

	        $di->set('carbon', new Carbon());

            $config = $di->get('config');

            $prefix = 'user/';

            $di->set('url', function() use ($config, $prefix) {

                $url = new Url();
                $url->setBaseUri($config->url . $prefix);
                $url->setStaticBaseUri($config->url);

                return $url;
            });

            /**
             * @var \Phalcon\Mvc\Dispatcher $dispatcher
             */
            $dispatcher = $di->get('dispatcher');

            $eventManager = $dispatcher->getEventsManager();

            $loginSecurity = new LoginSecurity();

            $eventManager->attach('dispatch:beforeDispatch', $loginSecurity);

            $di->set('login_security', $loginSecurity);

        }
    }
}
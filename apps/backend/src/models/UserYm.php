<?php

namespace App\Models;

use App\Model;
use App\Models\Events\CreateUpdateEvents;
use phpDocumentor\Reflection\Types\Integer;

/**
 * Class UserYm
 *
 * @package App\Models
 * @property integer $id
 * @property string  $ym_client_id
 * @property integer $user_id
 * @property string  $created_at
 * @property string  $updated_at
 *
 * @method static findFirstById($id)
 * @method static findFirstByUserId($user_id)
 * @method static findFirstByYmClientId($ym_client_id)
 */
class UserYm extends Model {

	use CreateUpdateEvents;

	public $id;

	public $ym_client_id;

	public $user_id;

	public $created_at;

	public $updated_at;

	public function initialize() {
		$this->belongsTo('user_id', User::class, 'id');
	}

}
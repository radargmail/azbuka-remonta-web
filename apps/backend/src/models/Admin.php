<?php

namespace App\Models;

use App\Model;
use App\Models\Events\CreateUpdateEvents;

/**
 * Class Admin
 *
 * @package App\Models
 * @property integer $id
 * @property string  $login
 * @property string  $password
 * @property string  $name
 * @property integer $status
 * @property integer $type
 * @property string  $email
 * @property string  $phone
 * @property integer $telegram_id
 * @property string  $created_at
 * @property string  $updated_at
 */
class Admin extends Model {

	const STATUS_ACTIVE   = 1;
	const STATUS_INACTIVE = 0;

	const TYPE_ADMIN  = 1;
	const TYPE_READER = 0;

	use CreateUpdateEvents;

	public $id;

	public $login;

	public $password;

	public $name;

	public $status;

	public $type;

	public $email;

	public $phone;

	public $telegram_id;

	public $created_at;

	public $updated_at;

	public function initialize() {}

}
<?php

namespace App\Models;

use App\Model;
use App\Models\Events\CreateUpdateEvents;
use Phalcon\Security\Random;
use phpDocumentor\Reflection\Types\Integer;

/**
 * Class User
 *
 * @package App\Models
 * @property integer $id
 * @property string  $login
 * @property string  $password
 * @property string  $last_name
 * @property string  $first_name
 * @property string  $middle_name
 * @property string  $avatar
 * @property string  $email
 * @property string  $phone
 * @property integer $status
 * @property string  $token
 * @property integer $telegram_id
 * @property string  $invite_code
 * @property integer $inviter_id
 * @property integer $is_change_password
 * @property string  $created_at
 * @property string  $updated_at
 *
 * @method static findFirstByEmail($email)
 * @method static findFirstByLogin($login)
 * @method static findFirstByToken(string $token)
 * @method static countByInviteCode($invite_code)
 * @method static findFirstByInviteCode($invite_code)
 * @method static findFirstById($user_id)
 * @method static findFirstByTelegramId($telegram_id)
 */
class User extends Model {

	const STATUS_ACTIVE   = 1;
	const STATUS_INACTIVE = 0;

	use CreateUpdateEvents;

	public $id;

	public $login;

	public $password;

	public $last_name;

	public $first_name;

	public $middle_name;

	public $avatar;

	public $email;

	public $phone;

	public $status;

	public $token;

	public $telegram_id;

	public $invite_code;

	public $inviter_id;

	public $is_change_password;

	public $created_at;

	public $updated_at;

	const GEN_INVITE = 0;
	const GEN_LOGIN = 1;
	const GEN_PASS = 2;

	const ALPHANUM_SYMBOLS = '123456789AaBbCcDdEeFfGgJjHhKkLMmNnQqPpRrSsTtUuVvYyWwXxZz';
	const SPECIAL_SYMBOLS = '%*)?@#$~';

	public function initialize() {}

	public function generateSymbolsString($length = 6, $type = self::GEN_INVITE, $with_special_symbols = false): bool {

		if(!$with_special_symbols) {
			$symbols_length = \strlen(self::ALPHANUM_SYMBOLS);
		} else {
			$symbols_length = \strlen(self::ALPHANUM_SYMBOLS . self::SPECIAL_SYMBOLS);
		}

		do {

			$generated = '';

			for($i = 0; $i < $length; $i++) {
				if(!$with_special_symbols) {
					$generated .= (self::ALPHANUM_SYMBOLS)[(new Random())->number($symbols_length)];
				} else {
					$generated .= (self::ALPHANUM_SYMBOLS . self::SPECIAL_SYMBOLS)[(new Random())->number($symbols_length)];
				}
			}

		} while(self::countByInviteCode($generated) !== 0);

		switch($type) {
			case self::GEN_INVITE:
				$this->invite_code = $generated;
				break;
			case self::GEN_LOGIN:
				$this->login = $generated;
				break;
			case self::GEN_PASS:
				$this->password = $generated;
				break;
		}

		return true;

	}

	public static function findFirstByPhone($phone) {

		return self::findFirst([
			'conditions' => 'phone = :phone:',
			'bind'       => [
				'phone' => $phone,
			],
		]);

	}

}
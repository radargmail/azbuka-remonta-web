<?php

namespace App\Models;

use App\Model;
use App\Models\Events\CreateUpdateEvents;

/**
 * Class AdminLogs
 *
 * @package App\Models
 * @property integer $id
 * @property integer $type
 * @property integer $admin_id
 * @property string  $ip
 * @property integer $action
 * @property object  $extras
 * @property string  $created_at
 */
class AdminLogs extends Model {

	use CreateUpdateEvents;

	public $id;

	public $type;

	public $admin_id;

	public $ip;

	public $action;

	public $extras;

	public $created_at;

	public function initialize() {
		$this->belongsTo('admin_id', Admin::class, 'id');
	}

}
<?php

namespace App\Models;

use App\Model;
use App\Models\Events\CreateUpdateEvents;

/**
 * Class EstimateTemp
 *
 * @package App\Models
 * @property integer $id
 * @property integer $estate_type
 * @property integer $repair_type
 * @property integer $design_type
 * @property integer $start_type
 * @property integer $worker_type
 * @property integer $area
 * @property integer $rooms
 * @property integer $user_id
 * @property string  $created_at
 * @property string  $updated_at
 */
class EstimateTemp extends Model {

	const ESTATE_NEW   = 0;
	const ESTATE_SECOND = 1;

	const REPAIR_CAPITAL   = 0;
	const REPAIR_COSMETIC = 1;

	const START_SOON   = 0;
	const START_ALREADY = 1;

	const WORKER_MYSELF   = 0;
	const WORKER_HIRED = 1;

	const DESIGN_STANDARD = 0;
	const DESIGN_DESIGNED = 1;

	use CreateUpdateEvents;

	public $id;

	public $estate_type;

	public $repair_type;

	public $design_type;

	public $start_type;

	public $worker_type;

	public $area;

	public $rooms;

	public $user_ym_id;

	public $created_at;

	public $updated_at;

	public function initialize() {
		$this->belongsTo('user_ym_id', UserYm::class, 'id');
	}

}
<?php

namespace App\Models\Events;

use Carbon\Carbon;
use DateTime;
use Phalcon\Mvc\Model\MetaDataInterface;

trait CreateUpdateEvents {

    protected function preSave(MetaDataInterface $metaData, $exists, $identityField): bool
    {

        if($exists) $this->updated_at = self::getNowDateTime();
        else $this->created_at = self::getNowDateTime();

        return parent::preSave($metaData, $exists, $identityField);

    }

    public static function getNowDateTime() {

        try {

            $dateTime = @DateTime::createFromFormat('U.u', microtime(true));

            if($dateTime){

                $date = $dateTime->format('Y-m-d H:i:s.u');

            } else {

                $date = @date('Y-m-d H:i:s', time());

            }
        } catch(\Exception $e){

            $date = @date('Y-m-d H:i:s', time());

        }

        return $date;
    }

}
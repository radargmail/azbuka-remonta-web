<?php

namespace App;

use App\Helpers\Formatters;
use Carbon\Carbon;
use ReflectionObject;
use ReflectionProperty;

class Model extends \Phalcon\Mvc\Model {

    protected $_formatters;

    public function format(): Formatters {
        $this->_formatters = $this->_formatters ?? new Formatters($this);

        return $this->_formatters;
    }

    public function getCarbon($field): Carbon {
        return Carbon::parse($this->{$field}, 'UTC');
    }

    public function fillByArray(array $data): void {

        $properties = (new ReflectionObject($this))->getProperties(ReflectionProperty::IS_PUBLIC);

        foreach($properties as $property){

            if(isset($data[ $property->name ])){

                if(empty($data[ $property->name ]) && $data[ $property->name ] !== '0' && $data[ $property->name ] !== 0){
                    $this->{$property->name} = null;
                } else {
                    $this->{$property->name} = $data[ $property->name ];
                }
            }
        }
    }

}
<?php

namespace App\Cli;


use App\Injectable;
use Crunz\Event;
use Phalcon\Di\FactoryDefault\Cli;

abstract class Scheduler extends Injectable {

    abstract public function init(Event $event): void;

    abstract public function run(): void;

    public function __construct(){

        require_once __DIR__ . '/../config/defines.php';

        $config = require CONFIG_PATH . 'config.php';

        $config->cli = true;

        require_once CONFIG_PATH . 'loader.php';

        require_once ROOT_PATH . 'vendor/autoload.php';

        $container = new Cli();

        require_once CONFIG_PATH . 'services.php';

        $container->set('config', $config);

        $this->setDI($container);

    }


}
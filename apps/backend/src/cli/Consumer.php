<?php

namespace App\Cli;

use App\Helpers\CliColors;
use Phalcon\Di;
use Throwable;

class Consumer {

    public function __construct() {
        $this->colors = new CliColors();
    }

    /**
     * @param $queueName
     * @throws Throwable
     */
    public function listen($queueName): void {

        $config = Di::getDefault()->get('config');

        /**
         * @var \App\Services\MessageQueue $queueContext
         */
        $queueContext = Di::getDefault()->get('messageQueue');

        $queue = $queueContext->createQueue($queueName);

        $consumer = $queueContext->createConsumer($queue);

        \Sentry\configureScope(function(\Sentry\State\Scope $scope) use ($queueName): void {
            $scope->setTag('queue_name', $queueName);
        });

        $this->print($queueName, 'started in ' . $config->env . ' environment', 'blue');
        $consoleTaskCounter = 0;

        /**
         * @var \Phalcon\Cli\Console $console
         */
        $console = Di::getDefault()->get('console');

        while(true){

            $message = $consumer->receive();

            if($message !== null && $message->getHeader('cli', false)){

                if($console){

                    $consoleTaskCounter++;

                    try {

                        $task = $message->getHeader('task', 'main');
                        $action = $message->getHeader('action', 'main');

                        $this->print($queueName . ' ' . $consoleTaskCounter, ' start handle ' . $task . ' ' . $action, 'blue');

                        $console->handle([
                            'task'   => $task,
                            'action' => $action,
                            'params' => $message->getProperties(),
                        ]);

                        $consumer->acknowledge($message);

                        $this->print($queueName . ' ' . $consoleTaskCounter, ' success handle', 'green');

                    } catch(Throwable $exception){

                        $this->print($queueName . ' ' . $consoleTaskCounter, ' error handle', 'red');
                        $this->print($queueName . ' ' . $consoleTaskCounter, $exception->getMessage(), 'red');

                        \Sentry\captureException($exception);

                        throw $exception;
                    }
                }
            }
        }
    }

    /**
     * Print colored text in console
     *
     * @param $title
     * @param $text
     * @param $color
     */
    public function print($title, $text, $color): void {
        echo ($title ? $this->colors->getColoredString($title . ' ', 'light_gray') : '') . $this->colors->getColoredString($text, $color) . PHP_EOL;
    }
}
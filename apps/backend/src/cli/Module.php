<?php

namespace App\Cli;

use Phalcon\DiInterface;
use Phalcon\Loader;
use Phalcon\Mvc\ModuleDefinitionInterface;

class Module implements ModuleDefinitionInterface {

    public function registerAutoloaders(DiInterface $dependencyInjector = null): void{

        $loader = new Loader();

        $loader->registerNamespaces([
            __NAMESPACE__ . '\Tasks' => __DIR__ . '/tasks/',
            __NAMESPACE__ . '\Schedulers' => __DIR__ . '/schedulers/',
        ]);

        $loader->register();

    }

    public function registerServices(DiInterface $dependencyInjector = null): void{

        if($dependencyInjector !== null){

            $view = $dependencyInjector->get('view');

            $view->setViewsDir(__DIR__ . '/views');

            $dependencyInjector->set('view', $view);

        }
    }
}
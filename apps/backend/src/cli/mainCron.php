<?php

use Crunz\Schedule;

require __DIR__ . '/../config/defines.php';

$config = require CONFIG_PATH . 'config.php';

require CONFIG_PATH . 'loader.php';

require ROOT_PATH . 'vendor/autoload.php';

$schedule = new Schedule();

foreach(new DirectoryIterator(__DIR__ . '/schedulers/') as $fileInfo){
    if($fileInfo->isDot()) continue;

    $classNamespace = '\\App\\Cli\\Schedulers\\' . pathinfo($fileInfo->getFilename())['filename'];

    $filePath = $fileInfo->getPath() . '/' . $fileInfo->getFilename();

    $event = $schedule->run(function() use ($classNamespace, $filePath) {

        require __DIR__ . '/../Injectable.php';
        require __DIR__ . '/Scheduler.php';
        require $filePath;

        /**
         * @var \App\Cli\Scheduler $scheduler
         */
        $scheduler = new $classNamespace();

        $scheduler->run();

    });

    $event->description($classNamespace)->preventOverlapping();

    /**
     * @var \App\Cli\Scheduler $scheduler
     */
    $scheduler = new $classNamespace();

    $scheduler->init($event);

}

return $schedule;


<?php

namespace App\Cli;

use App\Helpers\CliColors;
use Throwable;

/**
 * @property \Phalcon\Config                 $config
 * @property \Predis\Client                  $redis
 * @property \Phalcon\Logger\Adapter         $logger
 * @property \Phalcon\Mvc\View\Simple        $simpleView
 * @property \App\Helpers\RedisCache         $cache
 * @property \App\Services\MessageQueue      $messageQueue
 * @property \App\Services\FilesystemManager $filesystem
 * @property \App\Services\Mailer\Mailer     $mailer
 */
class Task extends \Phalcon\Cli\Task {

    public const SUCCESS_STATUS = 1;
    public const ERROR_STATUS = 0;

    /**
     * @var CliColors
     */
    public $colors;

    public function onConstruct(): void {

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 0);
        ini_set('display_errors', 1);
        set_time_limit(0);
        error_reporting(E_ALL);

        $this->colors = new CliColors();
    }

	/**
	 * @param $params
	 * @param $foundedParam
	 *
	 * @return bool
	 */
	public function issetInParams($params, $foundedParam): bool {
		if($params){
			foreach($params as $param){
				if($param === $foundedParam){
					return true;
				}
			}
		}

		return false;
	}

    public function mainAction($params = null): void {
        $this->print(null, 'This is the default task and the default action', 'blue');
    }

    /**
     * Print colored text in console
     *
     * @param $title
     * @param $text
     * @param $color
     */
    public function print($title, $text, $color): void {
        if($this->config->cli){
            echo ($title ? $this->colors->getColoredString($title . ' ', 'light_gray') : '') . $this->colors->getColoredString($text, $color) . PHP_EOL;
        } else {
            echo ($title ? '<span style="color: grey">' . $title . '</span>' : '') . '<span style="color: ' . $color . '">' . $text . '</span><br/>';
        }
    }

    public function success($message = null): int {
        if($message !== null) $this->print(static::class, $message, 'blue');

        return self::SUCCESS_STATUS;
    }

    public function error($errorMessage = null, $errorCode = self::ERROR_STATUS): int {
        if($errorMessage !== null) $this->print(static::class, $errorMessage, 'red');

        return $errorCode;
    }
}

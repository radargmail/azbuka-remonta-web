<?php

$options = [];

$options['environment'] = $config->env;

$options['dsn'] = $config->sentry->schema . '://' . $config->sentry->key . '@' .
    $config->sentry->host . '/' . $config->sentry->project_id;

Sentry\init($options);

Sentry\configureScope(function(Sentry\State\Scope $scope) use ($config): void {
    $scope->setTag('track_id', $config->track_id);
});
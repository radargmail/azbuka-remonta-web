<?php
declare(strict_types=1);

use App\Providers\ModulesProvider;
use Phalcon\Config;
use Phalcon\Debug;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Application;
use Phalcon\Mvc\Dispatcher;
use Sentry\SentrySdk;

/**
 * @var Config $config
 */
$config = require 'config.php';

if($config->debug) {

	ini_set('display_errors', '1');

	error_reporting(E_ALL);

	$debug = new Debug();

	$debug->listen();

	$_SERVER['start_time'] = microtime(true);
	$_SERVER['start_memory'] = memory_get_usage();

}

require ROOT_PATH . 'vendor/autoload.php';

require 'sentry.php';

/**
 * Add main dirs and namespaces in auto-loader
 */
$loader = require 'loader.php';

$container = new FactoryDefault();

$providers = require CONFIG_PATH . '/providers.php';
foreach($providers as $provider) {
	$container->register(new $provider());
}

$container->set('dispatcher', function() use ($container) {

	$eventsManager = $container->getShared('eventsManager');

	$eventsManager->attach('dispatch:beforeException', function($event, Dispatcher $dispatcher, Exception $exception) {
		switch($exception->getCode()) {
			case \Phalcon\Dispatcher\Exception::EXCEPTION_HANDLER_NOT_FOUND:
			case \Phalcon\Dispatcher\Exception::EXCEPTION_ACTION_NOT_FOUND:

				$dispatcher->forward([
					'controller' => 'errors',
					'action'     => 'show404',
				]);

				return false;
		}

		return true;
	});

	$dispatcher = new Dispatcher();
	$dispatcher->setEventsManager($eventsManager);

	return $dispatcher;

});

/**
 * Init application
 */

$application = new Application();

$application->setDI($container);

$application->registerModules(ModulesProvider::getModules());

try {

	$application->handle($_SERVER['REQUEST_URI'])->send();

} catch(Throwable $exception) {

	if(isset($debug)) {

		if($exception instanceof \Error) {
			$exception = new \ErrorException($exception->getMessage(), $exception->getCode(), \E_ERROR, $exception->getFile(), $exception->getLine());
		}

		/**
		 * @var Phalcon\Mvc\Dispatcher $dispatcher
		 */
		$dispatcher = $container->get('dispatcher');

		//if($dispatcher->getModuleName() !== null){
			$debug->onUncaughtException($exception);
		//}

	} else {

		Sentry\captureException($exception);

		$template = file_get_contents(ROOT_PATH . 'public/pages/wrong-with-track.html');

		$html = str_replace([
			'{track_id}',
			'{event_id}',
		], [
			$config->{'track_id'},
			SentrySdk::getCurrentHub()->getLastEventId(),
		], $template);

		echo $html;

	}

}
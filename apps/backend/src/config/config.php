<?php
$environment = getenv('EASURN_ENV') ?: 'dev';

$config = new \Phalcon\Config([

	'env'                  => $environment,
	'debug'                => false,
	'cli'                  => false,
	'url'                  => '/',
	'storage'              => [
		'type'     => 'local',
		// local || yandex,
		'private'  => [
			'bucket' => ROOT_PATH . '/private_file_storage/',
		],
		'public'   => [
			'bucket' => ROOT_PATH . '/public/public_file_storage',
			'prefix' => 'file_storage',
		],
		'endpoint' => ''
		// only for yandex
	],
	'database'             => [
		'postgres' => [
			'adapter'  => 'pgsql',
			'host'     => 'arweb_postgres',
			'dbname'   => 'arweb',
			'username' => 'arweb',
			'password' => '3PGgCFmNuIrSFGGH',
			'port'     => 5432,
		],
	],
	'application'          => [
		'namespaces' => [
			'App'                                    => __DIR__ . '/../',
			'App\Modules\Admin'                      => __DIR__ . '/../modules/admin/',
            'App\Modules\User'                       => __DIR__ . '/../modules/user/',
			'App\Models'                             => __DIR__ . '/../models/',
			'App\Models\Events'                      => __DIR__ . '/../models/events/',
			'App\Models\Statics'                     => __DIR__ . '/../models/statics/',
			'App\Models\Dictionaries'                => __DIR__ . '/../models/dictionaries/',
			'App\Models\Behaviors'                   => __DIR__ . '/../models/behaviors/',
			'App\Models\Traits'                      => __DIR__ . '/../models/traits/',
			'App\Models\Views'                       => __DIR__ . '/../models/views/',
			'App\Repositories'                       => __DIR__ . '/../repositories/',
			'App\Validators'                         => __DIR__ . '/../validators/',
			'App\Services'                           => __DIR__ . '/../services/',
			'App\Helpers'                            => __DIR__ . '/../helpers/',
			'App\Cli'                                => __DIR__ . '/../cli/',
			'App\Cli\Tasks'                          => __DIR__ . '/../cli/tasks/',
			'App\Cli\Schedulers'                     => __DIR__ . '/../cli/schedulers/',
			'App\Enums'                              => __DIR__ . '/../enums/',
			'App\Forms'                              => __DIR__ . '/../forms/',
			'App\Forms\Element'                      => __DIR__ . '/../forms/element',
			'App\Structures'                         => __DIR__ . '/../structures/',
			'App\Structures\Request'                 => __DIR__ . '/../structures/request',
			'App\Structures\Response'                => __DIR__ . '/../structures/response',
            'App\Behaviors'                          => __DIR__ . '/../behaviors/',
			'App\Providers'                          => __DIR__ . '/providers/',
		],
		'dirs'       => [],
	],
	'redis'                => [
		'host'     => 'redis',
		'port'     => 6379,
		'password' => 'A5MC87GcMJbQJT',
	],
	'queue'                => [
		'beanstalkd' => [
			'host' => 'beanstalkd',
			'port' => 11300,
		],
	],
	'logger'               => [
		'path'      => ROOT_PATH . 'logs/',
		'format'    => '%date% [%type%] %message%',
		'date'      => 'd.m.Y H:i:s',
		'logLevel'  => Phalcon\Logger::DEBUG,
		'extension' => '.log',
	],
	'secure'               => [
		'method' => 'AES-256-CTR',
		'vector' => 'U2mPw4S3dvk2tb6L3mzx3FpytaUR29CfX',
		'salt'   => '9m3fVMc@857Quf%1?gR#WtfC%8E%5s&cjPa77N2F!rgK^3B9HasuLYhuYz-aEZjFPBk',
	],
	'mail'                 => [
		'address'  => 'smtp.yandex.ru',
		'port'     => 465,
		'login'    => 'info@arweb.ru',
		'password' => '123456',
		'secure'   => 'ssl',
	],
	'sentry'               => [
		'schema'     => 'https',
		'host'       => 'o253298.ingest.sentry.io',
		'key'        => '3e69e5c7264145c3adcc6d3daefce82e',
		'project_id' => '6341160',
	],
]);

try {
	$config->track_id = (new \Phalcon\Security\Random())->uuid();
} catch(Exception $exception) {
	$config->track_id = uniqid('', true);
}

return $config;
<?php

use Phalcon\Loader;

$loader = new Loader();

foreach ($config->application->namespaces as $key => $value) {
    $loader->registerNamespaces([
        $key => $value
    ], true);
}

foreach ($config->application->dirs as $value) {
    $loader->registerDirs([
        $value
    ], true);
}

$loader->register();

return $loader;
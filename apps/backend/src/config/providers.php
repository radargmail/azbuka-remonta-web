<?php
declare(strict_types=1);

return [

	\App\Providers\ConfigProvider::class,
	\App\Providers\ModulesProvider::class,
	\App\Providers\RouterProvider::class,
	\App\Providers\UrlProvider::class,
	\App\Providers\EventsManagerProvider::class,
	\App\Providers\DbProvider::class,
	\App\Providers\ModelsCacheProvider::class,
	\App\Providers\CacheProvider::class,
	\App\Providers\ModelsMetadataProvider::class,
	\App\Providers\SessionProvider::class,
	\App\Providers\RedisProvider::class,
	\App\Providers\MessageQueueProvider::class,
	\App\Providers\ViewProvider::class,
	\App\Providers\VoltProvider::class,
	\App\Providers\AssetsProvider::class,
	\App\Providers\FlashSessionProvider::class,
	\App\Providers\SimpleViewProvider::class,
	\App\Providers\CryptProvider::class,
	\App\Providers\MailerProvider::class,
	\App\Providers\LoggerProvider::class,
	\App\Providers\FilesystemProvider::class,
	\App\Providers\ErrorTrackerProvider::class,

];
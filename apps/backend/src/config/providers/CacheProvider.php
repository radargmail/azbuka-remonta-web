<?php

declare(strict_types=1);

namespace App\Providers;

use App\Helpers\RedisCache;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class CacheProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'cache';

	public function register(DiInterface $di): void {

		$config = $di->getShared('config');
		$di->set(self::SERVICE_NAME, function() use ($config) {

			$options = [
				'defaultSerializer' => 'Php',
				'host'       => $config->redis->host,
				'auth'       => $config->redis->password,
				'port'       => $config->redis->port,
				'persistent' => 'first',
				'lifetime'   => 3600,
				'index'      => 0,
				'prefix'     => 'cache-',
			];

			$serializerFactory = new \Phalcon\Storage\SerializerFactory();

			$cache = new RedisCache($serializerFactory, $options);

			if($config->{'debug'}) {
				$cache->clear();
			}

			return $cache;

		});

	}

}
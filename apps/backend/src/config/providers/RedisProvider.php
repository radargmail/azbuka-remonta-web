<?php

declare(strict_types=1);

namespace App\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Predis\Client;

class RedisProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'redis';

	public function register(DiInterface $di): void {

		$config = $di->getShared('config');
		$di->set(self::SERVICE_NAME, function() use ($config) {

			return new Client([
				'host'       => $config->redis->host,
				'port'       => $config->redis->port,
				'password'   => $config->redis->password,
				'database'   => 0,
				'persistent' => 'first',
			]);

		});

	}

}
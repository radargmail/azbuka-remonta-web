<?php

declare(strict_types=1);

namespace App\Providers;

use App\Services\ErrorTracker;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class ErrorTrackerProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'errorTracker';

	public function register(DiInterface $di): void {

		$di->set(self::SERVICE_NAME, function() {

			return new ErrorTracker();

		});

	}

}
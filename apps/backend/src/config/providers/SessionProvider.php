<?php

declare(strict_types=1);

namespace App\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Session\Adapter\Redis;
use Phalcon\Session\Manager;
use Phalcon\Storage\AdapterFactory;
use Phalcon\Storage\SerializerFactory;

class SessionProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'session';

	public function register(DiInterface $di): void {

		$config = $di->getShared('config');
		$di->set(self::SERVICE_NAME, function() use ($config) {

			$options = [
				'prefix'     => 'sess-reds-',
				'host'       => $config->redis->host,
				'auth'       => $config->redis->password,
				'port'       => $config->redis->port,
				'persistent' => 'first',
				'lifetime'   => 3600*60*10,
				'index'      => 0,
			];

			$session = new Manager();
			$serializeFactory = new SerializerFactory();
			$factory = new AdapterFactory($serializeFactory);
			$redis = new Redis($factory, $options);

			$session->setAdapter($redis)->start();

			return $session;

		});

	}

}
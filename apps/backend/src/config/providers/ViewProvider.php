<?php

declare(strict_types=1);

namespace App\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Mvc\View;

class ViewProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'view';

	public function register(DiInterface $di): void {

		$di->setShared(self::SERVICE_NAME, function() {

			$view = new View();

			$view->registerEngines([
				'.volt' => 'volt',
			]);

			return $view;

		});

	}

}
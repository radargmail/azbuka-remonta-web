<?php

declare(strict_types=1);

namespace App\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Mvc\Router;

class RouterProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'router';

	public function register(DiInterface $di): void {

		$modules = $di->get('modules');
		$di->set(self::SERVICE_NAME, function() use ($modules) {

			$router = new Router();

			$router->removeExtraSlashes(true);

			foreach($modules as $key => $module){

				$prefix = $module['module_prefix'] ?? '';

				$router->add('/' . $prefix . $key, [
					'namespace'  => $module['namespace'] . '\Controllers',
					'module'     => $key,
					'controller' => 'index',
					'action'     => 'index',
				]);

				$router->add('/' . $prefix . $key . '/:controller', [
					'namespace'  => $module['namespace'] . '\Controllers',
					'module'     => $key,
					'controller' => 1,
				]);

				$router->add('/' . $prefix . $key . '/:controller/:action/:params', [
					'namespace'  => $module['namespace'] . '\Controllers',
					'module'     => $key,
					'controller' => 1,
					'action'     => 2,
					'params'     => 3,
				]);

				$router->add('/{name}', [
					'namespace'  => 'App\Modules\User\Controllers',
					'module'     => 'user',
					'controller' => 'index',
					'action'     => 'main',
				]);

			}

			foreach($modules as $key => $module){

				$router->setDefaultModule('user');

				$router->setDefaultController($modules['user']['namespace'] . '\Controllers\Index');

			}

			return $router;

		});

	}

}
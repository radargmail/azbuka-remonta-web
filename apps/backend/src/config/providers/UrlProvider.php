<?php

declare(strict_types=1);

namespace App\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Url;

class UrlProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'url';

	public function register(DiInterface $di): void {

		$config = $di->getShared('config');

		$di->set(self::SERVICE_NAME, function() use ($config) {
			$url = new Url();
			$url->setBaseUri($config->url);
			$url->setStaticBaseUri($config->url);

			return $url;
		});
	}
}
<?php

declare(strict_types=1);

namespace App\Providers;

use App\Services\Mailer\MailerService;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class MailerProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'mailer';

	public function register(DiInterface $di): void {

		$config = $di->getShared('config');
		$di->set(self::SERVICE_NAME, function() use ($config) {

			$service = new MailerService($config->mail->address, $config->mail->port, $config->mail->login, $config->mail->password, $config->mail->secure);

			return $service->getMailer();

		});

	}

}
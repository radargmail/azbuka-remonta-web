<?php

declare(strict_types=1);

namespace App\Providers;

use Phalcon\Crypt;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class CryptProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'crypt';

	public function register(DiInterface $di): void {

		$config = $di->getShared('config');
		$di->set(self::SERVICE_NAME, function() use ($config) {

			$crypt = new Crypt();
			$salt = substr($config->secure->salt, 0, 32);
			$crypt->setKey($salt);

			return $crypt;

		});

	}

}
<?php

declare(strict_types=1);

namespace App\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class ConfigProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'config';

	public function register(DiInterface $di): void {

		$environment = getenv('ARWEB_ENV') ?: 'dev';

		$configPath = CONFIG_PATH . 'config.php';
		$di->setShared(self::SERVICE_NAME, function() use ($configPath, $environment) {

			$config = require $configPath;

			/**
			 * Merge with env config
			 */
			if($environment !== 'my') {
				$config_file_name = CONFIG_PATH . "env/config.$environment.php";
				if(file_exists($config_file_name)) {
					$config->merge(require $config_file_name);
				}
			}

			/**
			 * Merge with my local config
			 */
			if(file_exists(CONFIG_PATH . 'env/config.my.php')) {
				$config->merge(require CONFIG_PATH . 'env/config.my.php');
			}

			return $config;

		});

	}

}
<?php

declare(strict_types=1);

namespace App\Providers;

use Phalcon\Db\Adapter\Pdo\Postgresql;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Migrations\Db\Dialect\DialectPostgresql;

class DbProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'db';

	public function register(DiInterface $di): void {

		$config = $di->getShared('config');
		$eventsManager = $di->getShared('eventsManager');
		$di->setShared(self::SERVICE_NAME, function() use ($config, $eventsManager) {

			$dialect = new DialectPostgresql();

			$dialect->registerCustomFunction(
				'UTF',
				function(DialectPostgresql $dialect, $expression) {
					$arguments = $expression['arguments'];
					return sprintf(
						'%s COLLATE "C.UTF-8"',
						$dialect->getSqlExpression($arguments[0])
					);
				}
			);

			$dialect->registerCustomFunction(
				'INTERVAL',
				function(DialectPostgresql $dialect, $expression) {
					$arguments = $expression['arguments'];
					return sprintf(
						'interval %s',
						$dialect->getSqlExpression($arguments[0])
					);
				}
			);

			$dialect->registerCustomFunction(
				'IF',
				function(DialectPostgresql $dialect, $expression) {
					$arguments = $expression['arguments'];
					return sprintf(
						'CASE WHEN %s THEN %s ELSE %s END',
						$dialect->getSqlExpression($arguments[0]),
						$dialect->getSqlExpression($arguments[1]),
						$dialect->getSqlExpression($arguments[2])
					);
				}
			);

			$discriptor = [
				'host'         => $config->database->postgres->host,
				'username'     => $config->database->postgres->username,
				'password'     => $config->database->postgres->password,
				'dbname'       => $config->database->postgres->dbname,
				'port'         => $config->database->postgres->port,
				'dialectClass' => $dialect
			];

			if(isset($config->database->postgres->dsn)){
				$discriptor['dsn'] = $config->database->postgres->dsn;
			}

			$connection = new Postgresql($discriptor);

			$connection->setEventsManager($eventsManager);

			return $connection;

		});

	}

}
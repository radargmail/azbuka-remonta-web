<?php

declare(strict_types=1);

namespace App\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Events\Manager;

class EventsManagerProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'eventsManager';

	public function register(DiInterface $di): void {

		$di->setShared(self::SERVICE_NAME, function() {

			return new Manager();

		});

	}

}
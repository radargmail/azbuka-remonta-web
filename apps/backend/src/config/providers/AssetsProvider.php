<?php

declare(strict_types=1);

namespace App\Providers;

use App\Helpers\AssetsManager;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class AssetsProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'assets';

	public function register(DiInterface $di): void {

		$di->set(self::SERVICE_NAME, function() {

			return new AssetsManager();

		});

	}

}
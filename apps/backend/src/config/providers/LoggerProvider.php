<?php

declare(strict_types=1);

namespace App\Providers;

use Carbon\Carbon;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Logger\Adapter\Stream;
use Phalcon\Logger\Formatter\Line;

class LoggerProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'logger';

	public function register(DiInterface $di): void {

		$config = $di->getShared('config');
		$di->set(self::SERVICE_NAME, function($filename = null, $format = null) use ($config) {

			$format = $format ?: $config->logger->format;
			$filename = trim($filename ?: Carbon::today()->toDateString() . $config->logger->extension, '\\/');

			$path = rtrim($config->logger->path, '\\/') . DIRECTORY_SEPARATOR;
			if(!is_dir($path) && !mkdir($path, 0755, true) && !is_dir($path)){
				throw new \RuntimeException(sprintf('Directory "%s" was not created', $path));
			}

			$formatter = new Line($format, $config->logger->date);

			$logger = new Stream($path . $filename);
			$logger->setFormatter($formatter);
			$logger->setLogLevel($config->logger->logLevel);

			return $logger;

		});

	}

}
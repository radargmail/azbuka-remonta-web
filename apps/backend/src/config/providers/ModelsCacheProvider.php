<?php

declare(strict_types=1);

namespace App\Providers;

use App\Helpers\RedisCache;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Storage\SerializerFactory;

class ModelsCacheProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'modelsCache';

	public function register(DiInterface $di): void {

		$config = $di->getShared('config');
		$di->set(self::SERVICE_NAME, function() use ($config) {

			$options = [
				'defaultSerializer' => 'Php',
				'host'       => $config->redis->host,
				'auth'       => $config->redis->password,
				'port'       => $config->redis->port,
				'persistent' => 'first',
				'lifetime'   => 186400,
				'index'      => 0,
				'prefix'     => 'models_сache-',
			];

			$serializerFactory = new SerializerFactory();

			$cache = new RedisCache($serializerFactory, $options);

			if($config->{'debug'}) {
				$cache->clear();
			}

			return $cache;

		});

	}

}
<?php

declare(strict_types=1);

namespace App\Providers;

use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Mvc\View\Simple;

class SimpleViewProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'simpleView';

	public function register(DiInterface $di): void {

		$di->set(self::SERVICE_NAME, function() {

			$view = new Simple();

			$view->registerEngines([
				'.volt' => 'volt',
			]);

			return $view;

		});

	}

}
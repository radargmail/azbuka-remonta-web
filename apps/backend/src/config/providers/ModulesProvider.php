<?php

declare(strict_types=1);

namespace App\Providers;

use App\Modules\Admin\Module as AdminModule;
use App\Modules\User\Module as UserModule;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class ModulesProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'modules';

	public function register(DiInterface $di): void {

		$di->set(self::SERVICE_NAME, function() {

			return self::getModules();

		});

	}

	public static function getModules() {

		return [
			'user' => [
				'namespace' => 'App\Modules\User',
				'className' => UserModule::class,
				'path'      => __DIR__ . '/../../modules/user/Module.php',
			],
			'admin' => [
				'namespace' => 'App\Modules\Admin',
				'className' => AdminModule::class,
				'path'      => __DIR__ . '/../../modules/admin/Module.php',
			],
		];

	}

}
<?php

declare(strict_types=1);

namespace App\Providers;

use App\Services\MessageQueue;
use Enqueue\Pheanstalk\PheanstalkConnectionFactory;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class MessageQueueProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'messageQueue';

	public function register(DiInterface $di): void {

		$config = $di->getShared('config');
		$di->setShared(self::SERVICE_NAME, function() use ($config) {

			$factory = new PheanstalkConnectionFactory([
				'host' => $config->queue->beanstalkd->host,
				'port' => $config->queue->beanstalkd->port,
			]);

			return new MessageQueue($factory->createContext());

		});

	}

}
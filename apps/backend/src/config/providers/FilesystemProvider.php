<?php

declare(strict_types=1);

namespace App\Providers;

use App\Services\Filesystem\PrivateFilesystem;
use App\Services\Filesystem\PublicFilesystem;
use App\Services\FilesystemManager;
use Aws\S3\S3Client;
use League\Flysystem\Adapter\Local;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Cached\CachedAdapter;
use League\Flysystem\Cached\Storage\Predis;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;

class FilesystemProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'filesystem';

	public function register(DiInterface $di): void {

		$config = $di->getShared('config');
		$redisCache = $di->get('redis');
		$di->set(self::SERVICE_NAME, function() use ($config, $redisCache) {

			$storageConfig = $config->storage;

			if($storageConfig->type === 'yandex'){

				$client = new S3Client([
					'credentials' => [
						'key'    => 'l9GPAxVmd_c_ET3NLHul',
						'secret' => 'TaAZ6FzS8XPZsk_lyB3T7idDN6fN9U8ljTcpKymF',
					],
					'region'      => 'us-east-1',
					'version'     => 'latest',
					'endpoint'    => $storageConfig->endpoint,
				]);

				$privateAdapter = new AwsS3Adapter($client, $storageConfig->private->bucket);
				$publicAdapter = new AwsS3Adapter($client, $storageConfig->public->bucket);

			} else {

				$privateAdapter = new Local($storageConfig->private->bucket);
				$publicAdapter = new Local($storageConfig->public->bucket);

			}

			if($config->debug){
				$privateAdapter = new CachedAdapter($privateAdapter, new Predis($redisCache));
				$publicAdapter = new CachedAdapter($publicAdapter, new Predis($redisCache));
			}

			$privateFilesystem = new PrivateFilesystem($privateAdapter);
			$publicFilesystem = new PublicFilesystem($publicAdapter, null, $storageConfig->public->prefix);

			return new FilesystemManager($privateFilesystem, $publicFilesystem);

		});

	}

}
<?php

declare(strict_types=1);

namespace App\Providers;

use Carbon\Carbon;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Mvc\View\Engine\Volt;

class VoltProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'volt';

	public function register(DiInterface $di): void {

		$config = $di->getShared('config');
		$view = $di->getShared('view');
		$module = $di->getShared('router')->getModuleName();
		$di->set(self::SERVICE_NAME, function() use ($config, $view, $module, $di) {

			$volt = new Volt($view, $di);
			$dir = ROOT_PATH . '/cache/templates/' . $module . '/';

			$volt->setOptions([
				'always' => true,
				'path' => function($templatePath) use ($dir) {

					if(!is_dir($dir) && !mkdir($dir, 0755, true) && !is_dir($dir)){
						throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
					}

					return $dir . md5($templatePath) . '.compiled';
				},
			]);

			$compiler = $volt->getCompiler();

			$compiler->addFunction('dump', 'dump');
			$compiler->addFunction('is_array', 'is_array');
			$compiler->addFunction('in_array', 'in_array');
			$compiler->addFunction('round', 'round');
			$compiler->addFunction('abs', 'abs');
			$compiler->addFunction('base64_encode', 'base64_encode');
			$compiler->addFunction('strtotime', 'strtotime');
			$compiler->addFunction('dechex', 'dechex');
			$compiler->addFunction('strtoupper', 'strtoupper');
			$compiler->addFunction('explode', 'explode');
			$compiler->addFunction('substr', 'mb_substr');
			$compiler->addFunction('carbon', function() {
				return new Carbon();
			});

			return $volt;

		});

	}

}
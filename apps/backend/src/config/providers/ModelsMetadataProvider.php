<?php

declare(strict_types=1);

namespace App\Providers;

use Phalcon\Cache\AdapterFactory;
use Phalcon\Di\DiInterface;
use Phalcon\Di\ServiceProviderInterface;
use Phalcon\Mvc\Model\MetaData\Redis;
use Phalcon\Storage\SerializerFactory;

class ModelsMetadataProvider implements ServiceProviderInterface {

	public const SERVICE_NAME = 'modelsMetadata';

	public function register(DiInterface $di): void {

		$config = $di->getShared('config');
		$di->set(self::SERVICE_NAME, function() use ($config) {

			$config = [
				'statsKey'   => '2sFs?3',
				'host'       => $config->redis->host,
				'auth'       => $config->redis->password,
				'port'       => $config->redis->port,
				'persistent' => 'first',
				'lifetime'   => 172800,
				'index'      => 0,
				'prefix'     => 'models_metadata-',
			];

			return new Redis(new AdapterFactory(new SerializerFactory()), $config);

		});

	}

}
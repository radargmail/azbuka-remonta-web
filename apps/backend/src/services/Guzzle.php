<?php

namespace App\Services;

use App\Service;
use Carbon\Carbon;
use GuzzleHttp\Client;

class Guzzle extends Service {

    protected $host;
    protected $path;
    protected $headers;
    protected $client;

    public function __construct($host, $headers) {

        $this->host    = $host;
        $this->headers = $headers;

        $hostParts  = explode('/', $this->host);
        $name       = $hostParts[2] ?? $hostParts[0];
        $this->path = 'guzzle/' . $name . '/';

        $this->client = new Client([
            'base_uri' => $this->host,
            'timeout'  => 6.0,
            'headers'  => $headers,
        ]);

    }

    /**
     * @param $method
     * @param $url
     * @param null $json
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    protected function request($method, $url, $json = null) {

        $body = [];
        if ($json !== null) {
            $body['json'] = $json;
        }

	    $today = Carbon::now()->format('d-m-Y');

        try {
            $response = $this->client->request($method, $url, $body);

            $responseBody = $response->getBody();

            if($this->filesystem->private->has($this->path . $url . '/' . $today . '.log')) {
            	
	            $this->filesystem->private->put(
	                $this->path . $url . '/' . $today . '.log',
	                'Time: ' . Carbon::now()->format('H:i:s') . PHP_EOL
	                . 'Request:' . $method . ' ' . $this->host . $url . ' ' . json_encode($body) . PHP_EOL
	                . 'Response:' . $response->getStatusCode() . ' ' . $responseBody . PHP_EOL 
		            . $this->filesystem->private->getContents($this->path . $url . '/' . $today . '.log')
	            );
	            return json_decode($responseBody, true);

            }

	        $this->filesystem->private->put(
		        $this->path . $url . '/' . $today . '.log',
		        'Time: ' . Carbon::now()->format('H:i:s') . PHP_EOL
		        . 'Request:' . $method . ' ' . $this->host . $url . ' ' . json_encode($body) . PHP_EOL
		        . 'Response:' . $response->getStatusCode() . ' ' . $responseBody . PHP_EOL
	        );
            return json_decode($responseBody, true);

        } catch (\Exception $e) {

	        if($this->filesystem->private->has($this->path . $url . '/' . $today . '.log')) {

		        $this->filesystem->private->put(
			        $this->path . $url . '/' . $today . '.log',
			        'Time: ' . Carbon::now()->format('H:i:s') . PHP_EOL
			        . 'Request:' . $method . ' ' . $this->host . $url . ' ' . json_encode($body) . PHP_EOL
			        . 'Response:' . $e->getMessage() . PHP_EOL
			        . $this->filesystem->private->getContents($this->path . $url . '/' . $today . '.log')
		        );
		        throw new \Exception($e->getMessage());

	        }

            $this->filesystem->private->put(
                $this->path . $url . '/' . $today . '.log',
	            'Time: ' . Carbon::now()->format('H:i:s') . PHP_EOL
                . 'Request:' . $method . ' ' . $this->host . $url . ' ' . json_encode($body) . PHP_EOL
                . 'Response:' . $e->getMessage() . PHP_EOL
            );
            throw new \Exception($e->getMessage());

        }

    }

}
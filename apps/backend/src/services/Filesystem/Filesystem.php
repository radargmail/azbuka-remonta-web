<?php

namespace App\Services\Filesystem;


use App\Helpers\ImageHelper;
use App\Models\User;
use League\Flysystem\AdapterInterface;
use League\Flysystem\FileExistsException;
use Phalcon\Http\Request\File;

abstract class Filesystem extends \League\Flysystem\Filesystem {

    public function __construct(AdapterInterface $adapter, $config = null) {

        parent::__construct($adapter, $config);

    }

    abstract public function getName(): string;

    abstract public function url($path, $downloadName = true): string;

    /**
     * Write phalcon file
     * @param       $path
     * @param File  $file
     * @param array $config
     * @return bool
     * @throws \League\Flysystem\FileExistsException
     */
    public function writeFile($path, File $file, array $config = []): bool {

        return $this->writeAndDeleteTmp($file->getTempName(), $path, $config);

    }

    /**
     * @param string $name "example_dir/example_name"
     * @param File   $file
     * @param User   $legalUser
     * @param array  $config
     *
     * @return false|string
     */
    public function writeFileByLegalUser($name, File $file, User $legalUser, array $config = []): ?string {
        if (array_key_exists('custom_name', $config)) {
            $fileClientNameHash = $this->getHashStr($config['custom_name']);
        } else {
            $fileClientNameHash = $this->getHashStr($file->getName());
        }


        $path = $this->getCompanyPath($legalUser->Office->company_id) . '/' . $legalUser->id . '/'
            . $name . '_' . $fileClientNameHash . '_' . dechex($file->getSize()) . '.' . strtolower($file->getExtension());

        try {

            if($this->writeFile($path, $file, $config)) return $this->getFilesystemPath($path);

        } catch(FileExistsException $e){
            return $this->getFilesystemPath($path);
        }

        return false;
    }

    /**
     * @param       $tmpPath
     * @param       $path
     * @param array $config
     * @return bool
     * @throws FileExistsException
     */
    public function writeAndDeleteTmp($tmpPath, $path, array $config = []): bool {

        $stream = fopen($tmpPath, 'rb+');

        $result = $this->writeStream($path, $stream, $config);

        if(\is_resource($stream)){
            fclose($stream);
        }

        unlink($tmpPath);

        return $result;
    }

    public function getCompanyPath($companyId): string {
        return 'companies/' . $companyId;
    }

    public function getOfficePath($officeId): string {
        return 'offices/' . $officeId;
    }

    public function getFilesystemPath($path): string {
        return $this->getName() . '://' . $path;
    }

    public function getCleanFilePath($path) {
        $explodedPath = explode('://', $path, 2);
        if(\count($explodedPath) > 1) return $explodedPath[1];

        return $path;
    }

    public function getImageHelper(): ImageHelper {
        return new ImageHelper($this);
    }

    public function getHashStr($str): string {
        return hash('crc32b', $str);
    }

    public function getContents($path) {
        $docPath = $this->getCleanFilePath($path);
        return $this->read($docPath);
    }
}
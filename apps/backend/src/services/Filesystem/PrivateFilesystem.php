<?php

namespace App\Services\Filesystem;

use Phalcon\Di;

class PrivateFilesystem extends Filesystem {

    /**
     * @param             $path
     * @param bool        $downloadName
     * @return bool
     */
    public function sendFile($path, $downloadName = true): bool {

        if($this->has($path)){

            $metadata = $this->getMetadata($path);

            header('Pragma: public');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Cache-Control: private', false);
            header('Content-Transfer-Encoding: binary');
            if($downloadName){
                header('Content-Disposition: attachment; filename="' .
                    (\is_bool($downloadName) ? basename($path) : $downloadName) . '";');
            }
            header('Content-Type: ' . $this->getMimetype($path));


            $chunkSize = 256 * 1024;
            $resource = $this->readStream($path);

            while(!feof($resource)){
                $buffer = fread($resource, $chunkSize);
                echo $buffer;
                ob_flush();
                flush();
            }

            if(is_resource($resource)) fclose($resource);

            return true;
        }

        return false;

    }

    public function url($path, $downloadName = true): string {

        $path = $this->getCleanFilePath($path);

        $url = Di::getDefault()->get('url');

        $params = [];

        $params['path'] = $path;
        $params['name'] = $downloadName;

        return $url->get('files/download') . '?' . http_build_query($params);
    }

    public function getName(): string {
        return 'private';
    }
}
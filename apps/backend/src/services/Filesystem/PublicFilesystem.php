<?php

namespace App\Services\Filesystem;

use League\Flysystem\AdapterInterface;
use Phalcon\Config;
use Phalcon\Di;
use Phalcon\Mvc\Url;

class PublicFilesystem extends Filesystem {

    protected $path;

    public function __construct(AdapterInterface $adapter, $config = null, $path = '') {

        parent::__construct($adapter, $config);

        $this->path = $path;

    }

    public function url($path, $downloadName = true): string {

        $path = $this->getCleanFilePath($path);

        /**
         * @var Url $url
         */
        $url = Di::getDefault()->get('url');

        /**
         * @var Config $config
         */
        $config = Di::getDefault()->get('config');

        $storageConfig = $config->storage;

        if($storageConfig->type === 'yandex'){

            return $storageConfig->endpoint . $storageConfig->public->bucket . '/' . $path;

        }

        return $url->getStatic($this->path . '/' . $path);
    }

    public function getName(): string {
        return 'public';
    }
}
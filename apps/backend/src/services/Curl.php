<?php

namespace App\Services;

use App\Service;
use Phalcon\Http\Response\Headers;

class Curl extends Service {


    public $debug = false;

    public $timeout = 30;

    protected $ch;

    protected $countRequests;

    protected $requestsTime;

    protected $_requestHeaders;

    protected $info;

    /**
     * @var Headers $_responseHeaders
     */
    protected $_responseHeaders;

    public function __construct($headers = null, $debug = false) {
        $this->debug = $debug;

        $this->ch = curl_init() or die("NO CURL!");

        curl_setopt($this->ch, CURLOPT_HEADER, 1);
        curl_setopt($this->ch, CURLOPT_ENCODING, 'deflate');
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, $this->timeout);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($this->ch, CURLINFO_HEADER_OUT, true);

        $this->setHeaders($headers);
    }

    function post($url, $data = false) {
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_POST, 1);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'POST');

        if($data) {
            $data = (is_array($data)) ? http_build_query($data) : $data;

            $this->_requestHeaders['Content-Length'] = 'Content-Length: ' . strlen($data);

            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $data);

            $this->lastData = $data;
        } else {

            $this->_requestHeaders['Content-Length'] = 'Content-Length: 0';

        }

        if($this->debug) {
            $this->logger->log('Request URL: ' . $url);
            if($data) {
                if(is_array($data))
                    $this->logger->log('Request body: ' . implode('; ', $data));
                else
                    $this->logger->log('Request body: ' . $data);
            }
        }

        return $this->exec();
    }

    function auth($login, $password){

        curl_setopt($this->ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($this->ch, CURLOPT_USERPWD, $login . ':'. $password);

    }

    function disableSSLVerification(){

        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);

    }

    function setSSLVersion($version){
        curl_setopt($this->ch, CURLOPT_SSLVERSION, $version);
    }

    function get($url) {
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($this->ch, CURLOPT_HTTPGET, 1);

        if($this->debug) {
            $this->logger->log('Request URL: ' . $url);
        }

        return $this->exec();
    }

    function exec() {

        if(!empty($this->_requestHeaders)) {

            curl_setopt($this->ch, CURLOPT_HTTPHEADER, $this->_requestHeaders);

            if($this->debug) $this->logger->log('Request headers: ' . implode("; ", $this->_requestHeaders));
        }

        $startTime = microtime(true);

        $res = curl_exec($this->ch);

        $this->requestsTime += microtime(true) - $startTime;

        $this->info = curl_getinfo($this->ch);

        $err = curl_error($this->ch);
        $errNo = curl_errno($this->ch);

        $rawHeaders = substr($res, 0, $this->info['header_size']);
        $body = substr($res, $this->info['header_size']);

        $headers = new Headers();
        foreach (explode("\r\n", $rawHeaders) as $i => $rawHeader) {
            $rawHeader = trim($rawHeader);
            if($rawHeader) {
                if($i === 0) $headers->set('code', $rawHeader);
                else {
                    $rawHeaderChunks = explode(': ', $rawHeader);
                    if(!isset($rawHeaderChunks[1])) $rawHeaderChunks[1] = '';
                    $headers->set($rawHeaderChunks[0], $rawHeaderChunks[1]);
                }
            }
        }
        $this->_responseHeaders = $headers;

        if($this->debug) {
            $this->logger->log('Error: ' . $err);
            $this->logger->log('Error No: ' . $errNo);
            $this->logger->log('Response headers: ' . $rawHeaders);
            $this->logger->log('Response body: ' . $body);
        }

        $this->countRequests++;

        return $body;
    }

    public function getRawInfo(){
        return $this->info;
    }

    public function setHeaders($headers) {
        $this->_requestHeaders = $headers;
    }

    public function getResponseHeaders() {
        return $this->_responseHeaders;
    }

    public function getRequestCount() {
        return $this->countRequests;
    }

    public function getRequestsTime() {
        return $this->requestsTime;
    }

    public function getLastData(){
        return $this->lastData;
    }

}
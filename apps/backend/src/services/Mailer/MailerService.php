<?php

namespace App\Services\Mailer;

use App\Service;
use Swift_Mailer;
use Swift_SmtpTransport;

class MailerService extends Service {

    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * MailerService constructor.
     * @param string $address
     * @param int    $port
     * @param null   $login
     * @param null   $password
     */
    public function __construct($address, $port = 25, $login = null, $password = null, $secure = null) {
        $this->setMailer($address, $port, $login, $password, $secure);
    }

    /**
     * @param string $address
     * @param int    $port
     * @param null   $login
     * @param null   $password
     * @param null   $secure
     */
    public function setMailer($address, $port = 25, $login = null, $password = null, $secure = null) {

        $transport = new Swift_SmtpTransport($address, $port, $secure);

        if($login !== null )$transport->setUsername($login);
        if($password !== null )$transport->setPassword($password);

        $this->mailer = new Mailer(new Swift_Mailer($transport));
    }

    /**
     * @return Mailer
     */
    public function getMailer() {
        return $this->mailer;
    }
}
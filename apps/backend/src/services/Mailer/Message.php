<?php

namespace App\Services\Mailer;

use Swift_Message;
/**
 * Class Message
 * @package App\Services\Mailer
 */
class Message {

    /**
     * @var Swift_Message
     */
    protected $swift;

    /**
     * Message constructor.
     * @param Swift_Message $swift
     */
    public function __construct(Swift_Message $swift) {
        $this->swift = $swift;
    }

    /**
     * @return Swift_Message
     */
    public function getSwiftMessage() {
        return $this->swift;
    }

    /**
     * @param array|string $from
     * @return Message
     */
    public function setFrom($from) {
        $this->swift->setFrom($from);
        return $this;
    }

    /**
     * @param array|string $to
     * @return Message
     */
    public function setTo($to) {
        $this->swift->setTo($to);
        return $this;
    }

    /**
     * @param string $subject
     * @return Message
     */
    public function setSubject($subject) {
        $this->swift->setSubject($subject);
        return $this;
    }

    /**
     * @param $body
     * @param $contentType
     * @return $this
     */
    public function setBody($body, $contentType) {
        $this->swift->setBody($body, $contentType);
        return $this;
    }
}
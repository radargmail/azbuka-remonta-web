<?php

namespace App\Services\Mailer;

use App\Models\MailLog;
use App\Service;
use Swift_Mailer;
use Swift_Message;

/**
 * Class Mailer
 * @package App\Services\Mailer
 */
class Mailer extends Service {

    /**
     * @var Swift_Mailer
     */
    protected $swift;

    protected $failedRecipients = null;

    /**
     * Mailer constructor.
     * @param Swift_Mailer $swift
     */
    public function __construct(Swift_Mailer $swift) {
        $this->swift = $swift;
    }

    /**
     * @param string $from
     * @param array $to
     * @param string $subject
     * @param string $body
     * @param null $contentType
     * @return int
     */
    public function send($from, $to, $subject, $body, $contentType = null) {
        $successfullNumber = 0;
        try {
            $message = $this->createMessage()
                ->setFrom($from)
                ->setTo($to)
                ->setSubject($subject)
                ->setBody($body, $contentType);

            $successfullNumber = $this->sendSwiftMessage($message->getSwiftMessage());
            $this->log($from, $to, $subject, $body);
        } catch (\Exception $exception) {
            $this->log($from, $to, $subject, $body, $exception->getMessage());
        }
        return $successfullNumber;
    }

    /**
     * @param Swift_Message $message
     * @return int
     */
    public function sendSwiftMessage(Swift_Message $message) {
        return $this->swift->send($message, $this->failedRecipients);
    }

    /**
     * @return Message
     */
    protected function createMessage() {
        return new Message(new Swift_Message);
    }

    /**
     * @return Swift_Mailer
     */
    public function getSwiftMailer() {
        return $this->swift;
    }

    /**
     * @param Swift_Mailer $swift
     */
    public function setSwiftMailer($swift) {
        $this->swift = $swift;
    }

    /**
     * @return null|array
     */
    public function getFailedRecipients() {
        return $this->failedRecipients;
    }

    /**
     * @param string $from
     * @param array $to
     * @param string $subject
     * @param string $body
     * @param string|null $error
     */
    protected function log($from, $to, $subject, $body, $error = null) {
        if(!\is_array($to)) $to = [$to];
        foreach($to as $recipient){
            if($error !== null || \in_array($recipient, $this->failedRecipients, true)){
                MailLog::error($from, $recipient, $subject, $body, $error);
            } else {
                MailLog::success($from, $recipient, $subject, $body);
            }
        }
    }
}
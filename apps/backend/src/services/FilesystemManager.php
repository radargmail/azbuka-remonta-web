<?php

namespace App\Services;


use App\Services\Filesystem\Filesystem;
use App\Services\Filesystem\PrivateFilesystem;
use App\Services\Filesystem\PublicFilesystem;
use League\Flysystem\FilesystemNotFoundException;
use League\Flysystem\MountManager;

class FilesystemManager extends MountManager {

    /**
     * @var PrivateFilesystem
     */
    public $private;

    /**
     * @var PublicFilesystem
     */
    public $public;

    public function __construct(PrivateFilesystem $privateFilesystem, PublicFilesystem $publicFilesystem) {

        $this->private = $privateFilesystem;
        $this->public = $publicFilesystem;

        parent::__construct([
            $privateFilesystem->getName() => $privateFilesystem,
            $publicFilesystem->getName()  => $publicFilesystem,
        ]);
    }

    public function url($path, $downloadName = true): string {

        try {
            [ $prefix, $filepath ] = $this->getPrefixAndPath($path);
        } catch(\Exception $exception){
            [ $prefix, $filepath ] = [ 'private', $path ];
        }

        /**
         * @var Filesystem $filesystem
         */

        $filesystem = $this->getFilesystem($prefix);

        return $filesystem->url($filepath, $downloadName);

    }

}
<?php

namespace App\Services;


use Interop\Queue\Consumer;
use Interop\Queue\Context;
use Interop\Queue\Destination;
use Interop\Queue\Exception;
use Interop\Queue\Exception\PurgeQueueNotSupportedException;
use Interop\Queue\Exception\SubscriptionConsumerNotSupportedException;
use Interop\Queue\Exception\TemporaryQueueNotSupportedException;
use Interop\Queue\Message;
use Interop\Queue\Producer;
use Interop\Queue\Queue;
use Interop\Queue\SubscriptionConsumer;
use Interop\Queue\Topic;

class MessageQueue implements Context {

    public const QUEUE_MAIN = 'main';
    public const QUEUE_MAIL = 'mail';
    public const QUEUE_TELEGRAM = 'telegram';

    /**
     * @var Context
     */
    private $context;

    public function __construct(Context $context) {
        $this->context = $context;
    }

    public function createMessage(string $body = '', array $properties = [], array $headers = []): Message {
        return $this->context->createMessage($body, $properties, $headers);
    }

    public function createTopic(string $topicName): Topic {
        return $this->context->createTopic($topicName);
    }

    public function createQueue(string $queueName): Queue {
        return $this->context->createQueue($queueName);
    }

    /**
     * Create temporary queue.
     * The queue is visible by this connection only.
     * It will be deleted once the connection is closed.
     *
     * @throws TemporaryQueueNotSupportedException
     */
    public function createTemporaryQueue(): Queue {
        return $this->context->createTemporaryQueue();
    }

    public function createProducer(): Producer {
        return $this->context->createProducer();
    }

    public function createConsumer(Destination $destination): Consumer {
        return $this->context->createConsumer($destination);
    }

    /**
     * @throws SubscriptionConsumerNotSupportedException
     */
    public function createSubscriptionConsumer(): SubscriptionConsumer {
        return $this->context->createSubscriptionConsumer();
    }

    /**
     * @throws PurgeQueueNotSupportedException
     */
    public function purgeQueue(Queue $queue): void {
        $this->context->purgeQueue($queue);
    }

    public function close(): void {
        $this->context->close();
    }

    public function getContext(): Context {
        return $this->context;
    }

    /**
     * @param string $queueName
     * @param string $task
     * @param string $action
     * @param array  $properties
     * @param array  $options "delay" - delay in milliseconds, timeToLive - in milliseconds, priority - in points
     *
     * @return Message
     * @throws Exception
     */
    public function callTaskAction(string $queueName, string $task, string $action = 'main', array $properties = [], array $options = []): Message {

        $producer = $this->createProducer();

        $queue = $this->createQueue($queueName);

        $message = $this->createMessage('', $properties, [
            'cli'    => true,
            'task'   => $task,
            'action' => $action,
        ]);

        if(isset($options['delay']) && (int)$options['delay'] > 0){
            $delay = (int)$options['delay'];
            if(method_exists($message, 'setDelay')) $message->setDelay($delay / 1000);
            else $producer->setDeliveryDelay($delay);
        }

        if(isset($options['priority']) && (int)$options['priority'] > 0){
            $priority = (int)$options['priority'];
            if(method_exists($message, 'setPriority')) $message->setPriority($priority);
            else $producer->setPriority((int)$options['delay']);
        }

        if(isset($options['timeToLive']) && (int)$options['timeToLive'] > 0){
            $timeToLive = (int)$options['timeToLive'];
            if(method_exists($message, 'setTimeToRun')) $message->setTimeToRun($timeToLive / 1000);
            else $producer->setTimeToLive((int)$options['timeToLive']);
        }

        $producer->send($queue, $message);

        return $message;
    }


}
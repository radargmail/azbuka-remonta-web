<?php

namespace App\Services;


use App\Service;
use Throwable;

class ErrorTracker extends Service {

    public function captureMessage($message, ...$options): void {
        \Sentry\captureMessage($message, ...$options);
    }

    public function captureException(Throwable $exception): void {
        \Sentry\captureException($exception);
    }

    public function captureEvent(array $array): void {
        \Sentry\captureEvent($array);
    }

}
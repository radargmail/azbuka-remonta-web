<?php

namespace App\Services\QueueTask;


use App\Services\MessageQueue;
use Phalcon\Di;

class Task implements TaskInterface {
    protected $name;
    protected $action;
    protected $params;
    protected $options = ['delay' => 0];

    public function __construct(string $name, string $action, array $params, array $options = null) {
        $this->name = $name;
        $this->action = $action;
        $this->params = $params;
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }


    public function call(): void {
        /**
         * @var MessageQueue $queue
         */
        $queue = Di::getDefault()->get('messageQueue');

        try {
            $queue->callTaskAction(
                MessageQueue::QUEUE_MAIL,
                $this->getName(),
                $this->getAction(),
                $this->getParams(),
                $this->getOptions()
            );
        } catch (\Exception $e) {
        }
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAction() {
        return $this->action;
    }

    /**
     * @param mixed $action
     */
    public function setAction($action): void {
        $this->action = $action;
    }

    /**
     * @return mixed
     */
    public function getParams() {
        return $this->params;
    }

    /**
     * @param mixed $params
     */
    public function setParams($params): void {
        $this->params = $params;
    }

    /**
     * @return mixed
     */
    public function getOptions() {
        return $this->options;
    }

    /**
     * @param mixed $options
     */
    public function setOptions($options): void {
        foreach ($options as $key => $option) {
            if ($key === 'delay') {
                $this->options['delay'] = $option;
            }

            if ($key === 'priority') {
                $this->options['priority'] = $option;
            }

            if ($key === 'timeToLive') {
                $this->options['timeToLive'] = $option;
            }
        }
    }

    public function getDelay() {
        return $this->options['delay'];
    }
}
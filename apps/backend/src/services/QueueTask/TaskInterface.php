<?php

namespace App\Services\QueueTask;


interface TaskInterface {
    public function getName();

    public function getAction();

    public function getParams();

    public function getOptions();

    public function getDelay();

    public function call();
}
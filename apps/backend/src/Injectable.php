<?php

namespace App;

/**
 * @property \Predis\Client                     $redis
 * @property \Phalcon\Config                    $config
 * @property \Phalcon\Logger\Adapter            $logger
 * @property \App\Helpers\RedisCache            $cache
 * @property \App\Services\MessageQueue         $messageQueue
 * @property \App\Services\FilesystemManager    $filesystem
 * @property \App\Services\ErrorTracker         $errorTracker
 * @property \App\Services\Mailer\Mailer        $mailer
 */
class Injectable extends \Phalcon\Di\Injectable {

}
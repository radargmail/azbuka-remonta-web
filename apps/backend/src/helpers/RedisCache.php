<?php

namespace App\Helpers;

use Closure;
use Phalcon\Cache\Adapter\Redis as RedisBackend;
use ReflectionFunction;

class RedisCache extends RedisBackend {

    public function call(Closure $callback, $lifeTime, $key = null) {

        if(empty($key)){
            try {
                $refCallback = new \ReflectionFunction($callback);
                $key = md5($refCallback->getStartLine() . $refCallback->getFileName() . $refCallback->getEndLine());
            } catch(\ReflectionException $e){
                $key = uniqid($lifeTime . '', true);
            }
        }

        $value = $this->get($key);

        if($value === null || empty($value)){
            $value = $callback();
            $this->set($key, $value, $lifeTime);
        }

        return $value;
    }

}
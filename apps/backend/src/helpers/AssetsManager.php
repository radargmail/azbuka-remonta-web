<?php

namespace App\Helpers;


use Phalcon\Assets\Collection;
use Phalcon\Assets\Filters\Cssmin;
use Phalcon\Assets\Filters\Jsmin;
use Phalcon\Assets\Manager;
use Phalcon\Di;
use Phalcon\Tag;

class AssetsManager extends Manager {

    public function output(Collection $collection, $callback, $type = null): string {

        $di = Di::getDefault();

        if(!$di->get('config')->debug){

            if(is_file($collection->getTargetPath())){

                /**
                 * Output 'min' file
                 */

                $modified = filemtime($collection->getTargetPath());

                if($type === 'css') return Tag::stylesheetLink($collection->getTargetUri() . '?m=' . $modified);

                if($type === 'js') return Tag::javascriptInclude($collection->getTargetUri() . '?m=' . $modified);

            } else {

                /**
                 * Create 'min' file and save and output
                 * If not exist file send raw collection
                 */

                if($collection->count() > 0){

                    if($collection->getTargetPath()){

                        $pathArr = explode('/', $collection->getTargetPath());

                        unset($pathArr[ count($pathArr) - 1 ]);

                        $path = implode('/', $pathArr);

                        if(!is_dir($path)){
                            mkdir($path, 0755, true);
                        }
                    }

                    try {

                        $minCollection = clone $collection;

                        if($type === 'js') $minCollection->join(true)->addFilter(new Jsmin());

                        if($type === 'css') $minCollection->join(true)->addFilter(new Cssmin());

                        return parent::output($minCollection, $callback, $type);


                    } catch(\Phalcon\Assets\Exception $e){

                        return parent::output($collection, $callback, $type);

                    }

                } else {

                    return '';

                }
            }

        } else {

            /**
             * If DEV_MODE is on, delete 'min' file. Output raw files
             */

            if(is_file($collection->getTargetPath())) unlink($collection->getTargetPath());

            return parent::output($collection, $callback, $type);
        }
    }

    public function createCollectionsByArray(array $arrayRawCollections, $type, $sourcePath = ROOT_PATH . 'public'): array {

        $collections = [];

        foreach($arrayRawCollections as $collectionName => $rawCollection){

            $collection = new Collection();

            $collection->setSourcePath($sourcePath . '/');

            if(isset($rawCollection['path'])){

                $filename = $rawCollection['filename'] ?? $collectionName;

                $targetFileName = $filename . '.min.' . $type;

                $collection->setTargetPath($sourcePath . '/' . $rawCollection['path'] . '/' . $targetFileName);
                $collection->setTargetUri('/' . $rawCollection['path'] . '/' . $targetFileName);
            }

            if(isset($rawCollection['target_filepath'])){

                $collection->setTargetPath($sourcePath . '/' . $rawCollection['target_filepath']);
                $collection->setTargetUri('/' . $rawCollection['target_filepath']);
            }

            if(isset($rawCollection['items'])){

                foreach($rawCollection['items'] as $item){

                    $local = null;
                    $filter = true;
                    $attributes = null;

                    if(\is_array($item)){

                        $path = $item['path'];

                        if(isset($item['local'])) $local = $item['local'];
                        if(isset($item['filter'])) $filter = $item['filter'];
                        if(isset($item['attributes'])) $attributes = $item['attributes'];

                    } else {

                        $path = $item;

                    }

                    if(file_exists($sourcePath . '/' . $path)){
                        if($type === 'css') $collection->addCss($path, $local, $filter, $attributes);
                        if($type === 'js') $collection->addJs($path, $local, $filter, $attributes);
                    }
                }
            }

            $collections[ $collectionName ] = $collection;

        }

        return $collections;
    }

    public function setCollections(array $collections): void {

        foreach($collections as $collectionId => $collection){
            $this->set($collectionId, clone $collection);
        }

    }
}
<?php

use App\Helpers\CliColors;
use Phalcon\Cli\Console;
use Phalcon\Cli\Dispatcher;
use Phalcon\Di\FactoryDefault\Cli;

require __DIR__ . '/src/config/defines.php';

$config = require CONFIG_PATH . 'config.php';

$config->cli = true;

require CONFIG_PATH . 'loader.php';

require 'vendor/autoload.php';

require CONFIG_PATH . 'sentry.php';

$container = new Cli();

$providers = require CONFIG_PATH . '/providers.php';
foreach($providers as $provider) {
	$container->register(new $provider());
}

$container->set('dispatcher', function() use ($container) {

	$eventsManager = $container->getShared('eventsManager');

	$eventsManager->attach('dispatch:beforeException', function($event, Dispatcher $dispatcher, Exception $exception) {
		switch($exception->getCode()) {
			case \Phalcon\Dispatcher\Exception::EXCEPTION_HANDLER_NOT_FOUND:
			case \Phalcon\Dispatcher\Exception::EXCEPTION_ACTION_NOT_FOUND:

				$dispatcher->forward([
					'controller' => 'errors',
					'action'     => 'show404',
				]);

				return false;
		}

		return true;
	});

	$dispatcher = new Dispatcher();
	$dispatcher->setEventsManager($eventsManager);

	return $dispatcher;

});

$console = new Console();

$console->setDI($container);

$arguments = [];

foreach($argv as $k => $arg){
    if($k === 1){
        $arguments['task'] = $arg;
    } else if($k === 2){
        $arguments['action'] = $arg;
    } else if($k >= 3){
        $arguments['params'][] = $arg;
    }
}

$container->set('console', $console);

try {

    $dispatcher = $container->get('dispatcher');

    $dispatcher->setDefaultNamespace('App\\Cli\\Tasks');
    $dispatcher->setNamespaceName('App\\Cli\\Tasks');

    $console->handle($arguments);

    exit(0);

} catch(\Exception $exception){

    Sentry\captureException($exception);

    $colors = new CliColors();

    if($exception->getCode() === 2) $error = 'Task "' . $arguments['task'] . '" undefined' . PHP_EOL;
    else if($exception->getCode() === 5) $error = 'Action "' . $arguments['action'] . '" in task "' . $arguments['task'] . '" undefined' . PHP_EOL;
    else $error = $exception->getMessage();

    echo $colors->getColoredString($error, 'red');

    exit(255);
}



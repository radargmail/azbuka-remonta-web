<?php

use App\Helpers\CliColors;
use App\Services\MessageQueue;
use Carbon\Carbon;
use Enqueue\Pheanstalk\PheanstalkConnectionFactory;
use Enqueue\Pheanstalk\PheanstalkMessage;

$queueName = $argv[1];

if(empty($queueName)){
    echo 'Queue name is required' . PHP_EOL;
    exit(255);
}

require __DIR__ . '/src/config/defines.php';

$config = require CONFIG_PATH . 'config.php';

$config->cli = true;

require CONFIG_PATH . 'loader.php';

require 'vendor/autoload.php';

require CONFIG_PATH . 'sentry.php';


$factory = new PheanstalkConnectionFactory([
    'host' => $config->queue->beanstalkd->host,
    'port' => $config->queue->beanstalkd->port,
]);

$queueContext = new MessageQueue($factory->createContext());

$queue = $queueContext->createQueue($queueName);

$consumer = $queueContext->createConsumer($queue);

$colors = new CliColors();
$print = function($text, $color) use ($colors): void {
    echo $colors->getColoredString(Carbon::now()->toDateTimeString(), 'light_gray') . ' ' . $colors->getColoredString($text, $color) . PHP_EOL;
};

$print('Started in ' . $config->env . ' environment', 'blue');
$consoleTaskCounter = 0;

while(true){

    try {

        /**
         * @var PheanstalkMessage $message
         */
        $message = $consumer->receive();

        if($message !== null && $message->getHeader('cli', false)){

            $consoleTaskCounter++;

            $task = $message->getHeader('task', 'main');
            $action = $message->getHeader('action', 'main');

            $print($consoleTaskCounter . ') Start handle ' . $task . ':' . $action, 'blue');

            $handleParams = [
                'task'   => $task,
                'action' => $action,
                'params' => $message->getProperties(),
            ];

            $handleStr = base64_encode(serialize($handleParams));

            $shell = 'php /var/www/cli.php main handle ' . $handleStr;

            $workerMessages = [];

            exec($shell, $workerMessages, $status);

            $print(
                $consoleTaskCounter . ') ' . ($status === 0 ? 'Successful' : 'Error') . ' handled ' . $task . ':' . $action . (count($workerMessages) > 0 ? ' with messages:' : ''),
                ($status === 0 ? 'blue' : 'red')
            );

            if(count($workerMessages) > 0) foreach($workerMessages as $workerMessage){
                echo '----| ' . $workerMessage . PHP_EOL;
            }

            if($status === 0){

                $consumer->acknowledge($message);

            } else {

                if(!$message->isRedelivered()){

                    $delayTime = 60;

                    $message->setDelay($delayTime);

                    $consumer->reject($message, true);

                    $print($consoleTaskCounter . ') Task ' . $task . ':' . $action . ' repeat after ' . $delayTime . 's in second time', 'blue');

                } else {

                    $consumer->reject($message);

                    $print($consoleTaskCounter . ') Task ' . $task . ':' . $action . ' rejected after 2 attempts', 'red');

                    $bot->writeOrEditMessageToMainChatInBackground(
                        $consoleTaskCounter . ') Task *' . $task . ':' . $action . '* rejected after *2* attempts',
                        null,
                        'markdown',
                        false, null, true
                    );

                }
            }
        }

    } catch(Throwable $exception){

        $print('CONSUMER ERROR: ' . $exception->getMessage(), 'red');

        \Sentry\captureException($exception);

        $bot->writeOrEditMessageToMainChatInBackground(
            '*CONSUMER ERROR*: ' . $exception->getMessage(),
            null,
            'markdown',
            false, null, true
        );
    }
}
(function (Url, Vue, $) {
    'use strict';

    var app = new Vue({
        el: '#construct',
        data: {
            estimates: [],
            newEstimate: [],
            id: 0,
            estate_type: 0,
            repair_type: 0,
            design_type: 0,
            start_type: 0,
            worker_type: 0,
            result_cost: 0,
            area: undefined,
            rooms: 1,
            user: [],
            uuid: false,
            ym_id: false,
            phone: false,
        },
        created: function () {
            let self = this;
            self.id = $('#form').data('id');
        },
        mounted: function () {
            let self = this;
            self.init();
            if(localStorage.getItem('estimates')) {
                try {
                    self.estimates = JSON.parse(localStorage.getItem('estimates'));
                    self.estate_type = self.estimates.estate_type;
                    self.repair_type = self.estimates.repair_type;
                    self.design_type = self.estimates.design_type;
                    self.start_type = self.estimates.start_type;
                    self.worker_type = self.estimates.worker_type;
                    self.area = self.estimates.area;
                    self.rooms = self.estimates.rooms;
                    self.uuid = self.estimates.user.uuid ? self.estimates.user.uuid : self.genUuid();
                    self.ym_id = self.estimates.user.ym_id;
                    self.phone = self.estimates.user.phone;
                    self.result_cost = self.estimates.result_cost;
                } catch (e) {
                    localStorage.removeItem('estimates');
                }
            } else {
                self.genUuid();
            }
        },
        methods: {

            init: function () {
                let self = this;
                setTimeout(function () {
                    if(!ClientId) {
                        ym(71274733, 'getClientID', function(clientID) {
                            ClientId = clientID;
                        });
                    }
                    if(localStorage.getItem('estimates') && !self.ym_id) {
                        localStorage.removeItem('estimates');
                    }
                    $('form').on('submit', function (e) {
                        //e.preventDefault();
                        self.newEstimate = {
                            estate_type: self.estate_type,
                            repair_type: self.repair_type,
                            design_type: self.design_type,
                            start_type: self.start_type,
                            worker_type: self.worker_type,
                            area: self.area,
                            rooms: self.rooms,
                            user: {
                                ym_id: ClientId,
                                uuid: self.uuid,
                                phone: self.phone,
                            }
                        }

                        self.resultCost();

                        setTimeout(function () {
                            console.log(self.result_cost);
                            if(self.result_cost > 0) {
                                self.newEstimate.result_cost = self.result_cost;
                                self.estimates = self.newEstimate;
                                self.newEstimate = [];
                                console.log(self.estimates);
                                self.saveEstimate();
                            } else {
                                e.preventDefault();
                            }
                        }, 100);
                    });
                }, 10);
            },

            saveEstimate: function () {
                let parsed = JSON.stringify(this.estimates);
                localStorage.setItem('estimates', parsed);
            },

            resultCost: function () {
                let self = this;
                $.ajax({
                    method: 'post',
                    url: Url.get('index/resultCost'),
                    data: self.newEstimate,
                    async: false,
                    cache: false,
                    timeout: 60000,
                    success: function (data) {
                        setTimeout(function () {
                            self.result_cost = data;
                        }, 10);
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
                return self.result_cost;
            },

            genUuid: function () {
                let self = this;
                if (!self.uuid) {
                    $.ajax({
                        method: 'get',
                        url: Url.get('index/genUuid'),
                        async: true,
                        cache: false,
                        contentType: false,
                        processData: false,
                        timeout: 60000,
                        success: function (data) {
                            setTimeout(function () {
                                self.uuid = data;
                            }, 10);
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                }
            },

        },
        watch: {
            /*type: function (val) {
                let self = this;
                self.reInitValidation();
            }*/
        }
    });

})(window.Url, window.Vue, window.$);
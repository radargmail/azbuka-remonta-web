
Vue.component('BootstrapModal', {
    props: {
        show: {
            type: Boolean,
            default: false
        },
        options: {
            type: Object,
            default: {}
        }
    },
    data: function(){
        return {
            modal: new BootstrapModal()
        };
    },
    template: '<slot></slot>',
    mounted: function (){
        this.modal = new BootstrapModal(this.options);
        this.modal.setContent(this.$el);
    },
    watch: {
        show: function(isShow){
            if(isShow){
                this.modal.show();
            }else{
                this.modal.hide();
            }
        }
    }
});
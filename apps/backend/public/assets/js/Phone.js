
var PhoneModal = (function(){

    'use strict';

    return {
        estimate: undefined,
        phone: undefined,
        modal: undefined,
        init: function() {
            let self = this;

            if(localStorage.getItem('estimates')) {
                self.estimate = JSON.parse(localStorage.getItem('estimates'));
                if(self.estimate.user.phone) {
                    self.phone = self.estimate.user.phone;
                }
            }

            return this;
        },
        render: function() {
            let self = this;
            self.modal = new BootstrapModal({
                backdrop: 'static'
            });

            let html = '';


            if (self.phone == undefined || self.phone == 'false') {

                html +=
                    '<div class="modal-content">' +
                        '<button type="button" class="close-modal" data-dismiss="modal" aria-label="Close">' +
                            '<i class="icon-cross2"></i>' +
                        '</button>'+
                        '<div class="text-center pt-3"><i class="fa fa-unlock-alt fa-2x border-3 rounded-pill p-3 mb-3" style="color:#37C7AB;border-color:#37C7AB;"></i></div>' +
                        '<div class="modal-header">' +
                            '<h3 class="modal-title font-weight-bold">Сохраните настройки и смело продолжайте!</h3>' +
                        '</div>' +
                        '<div class="modal-body mt-2">' +
                            '<h6 class="text-left mb-4">Пользуйтесь помощником в ремонте с любого устройства. Ваш прогресс будет доступен по номеру телефона.<br><br>Никаких паролей!</h6>' +
                            '<div class="row">' +
                                '<div class="col-12 text-center">' +
                                    '<div class="form-group form-group-feedback form-group-feedback-left" style="width:60%;margin:8px auto;">' +
                                        '<input type="text" required class="form-control form-control-lg text-right font-weight-bold font-size-lg arweb-phone-input" style="color:#37C7AB;" data-mask="+7 (999) 999-99-99" placeholder="+7 (900) 000-00-00" autofocus>' +
                                        '<div class="form-control-feedback arweb-phone-label form-control-feedback-lg">' +
                                            'Номер телефона' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                            '<div class="text-right pt-3">' +
                                // '<button type="button" class="btn btn-danger mr-2" style="border-radius: 8px;" data-dismiss="modal" aria-label="Close">Отмена</button>' +
                                '<button type="submit" class="btn ar-butts btn-primary">Сохранить и продолжить <i class="fa fa-long-arrow-alt-right"></i></button>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
            } else {
                html +=
                    '<div class="modal-content">' +
                        '<div class="modal-header">' +
                            '<h3 class="modal-title font-weight-bold">Спасибо!</h3>' +
                        '</div>' +
                        '<div class="modal-body mt-2">' +
                            '<h5 class="text-center">Сервис находится на стадии тестирования, мы сообщим Вам о его запуске!</h5>' +
                            '<div class="text-right pt-3">' +
                                '<button type="button" class="btn btn-info" style="border-radius: 8px;" data-dismiss="modal" aria-label="Close">Закрыть</button>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
            }


            let content = $(html),
                $buttonSubmit = content.find('button[type="submit"]'),
                $phone = content.find('input.arweb-phone-input');

            self.modal.setContent(content);
            self.modal.show();

            $buttonSubmit.click(function () {
                if($phone.val() !== '' && $phone.length != 18) {
                    let clerPhone = $phone.val().replace(/[^+\d]+/g, '');
                    $.ajax({
                        method: 'post',
                        url: Url.get('index/addPhone'),
                        data: {phone: clerPhone},
                        async: false,
                        cache: false,
                        timeout: 60000,
                        success: function (data) {
                            if (data.response == 'ok') {
                                self.phone = clerPhone;

                                let newData = JSON.parse(localStorage.getItem('estimates'));
                                console.log(newData);
                                newData.user.phone = self.phone;
                                ym(71274733,'reachGoal','arweb_user');
                                localStorage.setItem('estimates', JSON.stringify(newData));
                            }
                            console.log(data);
                            self.modal.hide();
                            self.render();
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                } else {
                    if(!$phone.hasClass('border-danger')) {
                        $phone.addClass('border-danger').after('<span class="font-size-xs text-danger">Не верно заполнен номер телефона!</span>');
                    }
                }
            });

        },
    };

})();
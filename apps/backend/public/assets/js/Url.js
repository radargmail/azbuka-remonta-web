
var Url = (function(){

    'use strict';

    var url = '';
    var staticUrl = '';

    return {
        get: function(path){
            return url + path;
        },
        getStatic: function(path){
            return staticUrl + path;
        },
        set: function(newUrl){
            url = newUrl;
        },
        setStatic: function(newUrl){
            staticUrl = newUrl;
        }
    };

})();
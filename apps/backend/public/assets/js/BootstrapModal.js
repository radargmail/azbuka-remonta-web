/**
 * Example:
 *
 * var options = {backdrop: 'static', keyboard: false}
 *
 * var modal = $('.modal-content').BootstrapModal(options);
 * modal.show();
 * modal.hide();
 * modal.setContent($element);
 *
 * var modalWrapper = modal.getModal();
 *
 * @param options
 * @returns {{show, hide, setContent, getModal}}
 * @constructor
 */

var BootstrapModal = (function($){
    'use strict';

    return function(options){

        var $div = $('<div>');

        var $modalWrapper = $div.clone().addClass('modal fade')
            .attr('role', 'dialog').attr('aria-hidden', 'true');
        var $modalDialog = $div.clone().addClass('modal-dialog modal-dialog-centered')
            .attr('role', 'document');

        if(options && typeof options.class !== 'undefined'){
            $modalDialog.addClass(options.class);
        }

        $modalWrapper.append($modalDialog);

        var methods = {
            show: function(){
                $modalWrapper.modal(options);
            },
            hide: function(){
                $modalWrapper.modal('hide');
            },
            setContent: function($element){
                $modalDialog.append($element);
            },
            getModal: function(){
                return $modalWrapper;
            }
        };

        $('body').append($modalWrapper);

        return methods;

    };

})($);

(function($){

    $.fn.BootstrapModal = function(options){

        var $this = $(this);

        var clone = true;

        if(typeof options !== "undefined" && typeof options.clone !== "undefined"){
            clone = options.clone;
        }

        var modal = BootstrapModal(options);

        if(clone){
            modal.setContent($this.clone());
            $this.remove();
        }else{
            modal.setContent($this);
        }

        return modal;
    };

}($));
<?php
declare(strict_types=1);

use Phalcon\Config;

ini_set('memory_limit', '-1');

require __DIR__ . '/../src/config/defines.php';

/**
 * @var Config $config
 */
$config = require CONFIG_PATH . 'bootstrap.php';
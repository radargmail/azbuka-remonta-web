<?php

use Phinx\Db\Adapter\PostgresAdapter;
use Phinx\Migration\AbstractMigration;

class StartMigration extends AbstractMigration {
	/**
	 * Change Method.
	 *
	 * Write your reversible migrations using this method.
	 *
	 * More information on writing migrations is available here:
	 * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
	 *
	 * The following commands can be used in this method and Phinx will
	 * automatically reverse them when rolling back:
	 *
	 *    createTable
	 *    renameTable
	 *    addColumn
	 *    addCustomColumn
	 *    renameColumn
	 *    addIndex
	 *    addForeignKey
	 *
	 * Any other destructive changes will result in an error when trying to
	 * rollback the migration.
	 *
	 * Remember to call "create()" or "update()" and NOT "save()" when working
	 * with the Table class.
	 */
	public function change() {

		$admin = $this->table('admin');
		$admin
			->changeComment('Администраторы системы')
			->addColumn('login', PostgresAdapter::PHINX_TYPE_STRING)
			->addColumn('password', PostgresAdapter::PHINX_TYPE_STRING)
			->addColumn('name', PostgresAdapter::PHINX_TYPE_STRING)
			->addColumn('status', PostgresAdapter::PHINX_TYPE_INTEGER)
			->addColumn('type', PostgresAdapter::PHINX_TYPE_INTEGER)
			->addColumn('email', PostgresAdapter::PHINX_TYPE_STRING)
			->addColumn('phone', PostgresAdapter::PHINX_TYPE_STRING, ['null' => true])
			->addColumn('telegram_id', PostgresAdapter::PHINX_TYPE_INTEGER, ['null' => true])
			->addColumn('created_at', PostgresAdapter::PHINX_TYPE_DATETIME)
			->addColumn('updated_at', PostgresAdapter::PHINX_TYPE_DATETIME, ['null' => true])

			->addIndex('login', ['unique' => true])
			->addIndex('email', ['unique' => true])
			->addIndex('phone', ['unique' => true])
			->addIndex('telegram_id')

			->create();

		$admin_log = $this->table('admin_log');
		$admin_log
			->changeComment('Логи действий администраторов')
			->addColumn('type', PostgresAdapter::PHINX_TYPE_STRING, ['default' => 'info', 'comment' => 'Тип записи в логах'])
			->addColumn('admin_id', PostgresAdapter::PHINX_TYPE_INTEGER)
			->addColumn('ip', PostgresAdapter::PHINX_TYPE_STRING, ['limit' => 40])
			->addColumn('action', PostgresAdapter::PHINX_TYPE_INTEGER, ['default' => 0, 'limit' => 4, 'comment' => 'ИД действия из справочника'])
			->addColumn('extras', PostgresAdapter::PHINX_TYPE_JSON, ['comment' => 'Дополнительные данные по тегам для автоподстановки в логи'])
			->addColumn('created_at', PostgresAdapter::PHINX_TYPE_DATETIME)

			->addForeignKey('admin_id', 'admin', 'id')

			->create();

		$user = $this->table('user');
		$user
			->changeComment('Пользователи системы')
			->addColumn('login', PostgresAdapter::PHINX_TYPE_STRING)
			->addColumn('password', PostgresAdapter::PHINX_TYPE_STRING)
			->addColumn('last_name', PostgresAdapter::PHINX_TYPE_STRING, ['default' => 'Фамилия'])
			->addColumn('first_name', PostgresAdapter::PHINX_TYPE_STRING, ['default' => 'Имя'])
			->addColumn('middle_name', PostgresAdapter::PHINX_TYPE_STRING, ['null' => true])
			->addColumn('avatar', PostgresAdapter::PHINX_TYPE_STRING, ['null' => true])
			->addColumn('email', PostgresAdapter::PHINX_TYPE_STRING)
			->addColumn('phone', PostgresAdapter::PHINX_TYPE_STRING)
			->addColumn('status', PostgresAdapter::PHINX_TYPE_INTEGER, ['default' => 0])
			->addColumn('token', PostgresAdapter::PHINX_TYPE_STRING, ['null' => true])
			->addColumn('telegram_id', PostgresAdapter::PHINX_TYPE_INTEGER, ['null' => true])
			->addColumn('invite_code', PostgresAdapter::PHINX_TYPE_STRING, ['null' => true])
			->addColumn('inviter_id', PostgresAdapter::PHINX_TYPE_STRING, ['null' => true])
			->addColumn('is_change_password', PostgresAdapter::PHINX_TYPE_INTEGER, ['default' => 0])
			->addColumn('created_at', PostgresAdapter::PHINX_TYPE_DATETIME)
			->addColumn('updated_at', PostgresAdapter::PHINX_TYPE_DATETIME, ['null' => true])

			->addIndex('login', ['unique' => true])
			->addIndex('email', ['unique' => true])
			->addIndex('phone', ['unique' => true])
			->addIndex('invite_code', ['unique' => true])
			->addIndex('telegram_id')

			->create();

		$user_ym = $this->table('user_ym');
		$user_ym
			->changeComment('Соотношение Пользователь и Яндекс ClientID')
			->addColumn('ym_client_id', PostgresAdapter::PHINX_TYPE_STRING)
			->addColumn('user_id', PostgresAdapter::PHINX_TYPE_INTEGER, ['null' => true])
			->addColumn('created_at', PostgresAdapter::PHINX_TYPE_DATETIME)
			->addColumn('updated_at', PostgresAdapter::PHINX_TYPE_DATETIME, ['null' => true])

			->addForeignKey('user_id', 'user', 'id')

			->addIndex('user_id')

			->create();

		$estimate = $this->table('estimate_temp');
		$estimate
			->changeComment('Сохраненные отчеты')
			->addColumn('estate_type', PostgresAdapter::PHINX_TYPE_INTEGER)
			->addColumn('repair_type', PostgresAdapter::PHINX_TYPE_INTEGER)
			->addColumn('design_type', PostgresAdapter::PHINX_TYPE_INTEGER)
			->addColumn('start_type', PostgresAdapter::PHINX_TYPE_INTEGER)
			->addColumn('worker_type', PostgresAdapter::PHINX_TYPE_INTEGER)
			->addColumn('area', PostgresAdapter::PHINX_TYPE_INTEGER)
			->addColumn('rooms', PostgresAdapter::PHINX_TYPE_INTEGER)
			->addColumn('user_ym_id', PostgresAdapter::PHINX_TYPE_INTEGER, ['null' => true])
			->addColumn('created_at', PostgresAdapter::PHINX_TYPE_DATETIME)
			->addColumn('updated_at', PostgresAdapter::PHINX_TYPE_DATETIME, ['null' => true])

			->addForeignKey('user_ym_id', 'user_ym', 'id')

			->addIndex('user_ym_id')

			->create();

	}
}
# Azbuka Remonta #

Local installation
------------

**Install Docker**

**Up docker**
`
docker-compose up --build
`

Development
-----------

**Local docker**

Start: `docker-compose up`

Stop: `docker-compose stop`

Restart container: `docker-compose restart <CONTAINER_NAME>`

Exec in container: `docker exec -it <CONTAINER_ID> bash`


-----------

**Composer**

Update composer:
`
docker exec -it arweb_backend composer update
`


-----------

**Phinx**

Create migration (see in `apps/backend/database/migrations`):
`
docker exec -it arweb_backend php vendor/bin/phinx create <NewMigrationName>
`

Migrate:
`
docker exec -it arweb_backend php vendor/bin/phinx migrate -e default
`

-----------


**Schedulers**

Scheduler list:

`
docker exec -it arweb_backend php vendor/bin/crunz schedule:list
`

---

Servers
-------

Dev start: `docker-compose -f docker-compose.dev.yml up --build -d`

Dev stop: `docker-compose -f docker-compose.dev.yml stop`

Prod start: `docker-compose -f docker-compose.prod.yml up --build -d`

Prod stop: `docker-compose -f docker-compose.prod.yml stop`



Backup
-------

Make backup:
`
docker exec -it arweb_postgres pg_dump -U arweb arweb > backup.sql
`

Restore backup:
`
docker exec -i arweb_postgres psql -U arweb --set ON_ERROR_STOP=on arweb < backup.sql
`